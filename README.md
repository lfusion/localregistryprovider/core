# LFusion Local Registry Provider

[[English]](/Documentation~/Local%20Registry%20Provider%20EN.md) 
[[Français]](/Documentation~/Local%20Registry%20Provider%20FR.md)

Ce package permet de créer un fournisseur de registre lié au projet.  
Les packages peuvent être ajoutés par des extensions implémentant un fournisseur.

 1. [Prérequis](#prérequis)
 2. [Installation](#installation)
 3. [Configuration](#configuration)
 4. [Safe Mode](#safe-mode)
 5. [LFusion Extensions](#lfusion-extensions)

## Prérequis

 - **Unity 2020** ou supérieur

## Installation

### Installer via Gitlab registry

 - Ajouter le registre [LFusion Upm-Registry](https://gitlab.com/lfusion/upm-registry)
 - **Pour Unity 2021 ou plus récent**
	 - Package Manager
	 - **\+** Add package by name...
		 - `Name : com.lfusion.localregistryprovider.core`
 - **Pour toutes les versions supportées**
	 - Ouvrir `manifest.json` dans le dossier Packages du projet.
	 - Ajouter `"com.lfusion.localregistryprovider.core": "x.x.x",` dans le bloc `dependencies`  en remplaçant `x.x.x` par la version voulu.
	 - Exemple pour la version `1.0.0` :
```json
{
  "dependencies": {
    "com.lfusion.localregistryprovider.core": "1.0.0",
    [...]
  },
  [...]
}
```

### Installer via tarball

 - Télécharger `com.lfusion.localregistryprovider.core-x.x.x.tgz` depuis [releases](https://gitlab.com/lfusion/localregistryprovider/core/-/releases).
 - Package Manager
 - **\+** Add package from tarball...
 - Sélectionnez  `com.lfusion.localregistryprovider.core-x.x.x.tgz`

## Configuration

Les paramètres de LFusion Local Registry Provider sont accessibles depuis:   

 1. `Edit/Project Settings...`
 2. `Package Manager/Local Registry`

### Package Mode

Indique le mode de fourniture des packages, *Non Embarqué* (serveur local) ou *Embarqué*.  
Permet de basculer le mode de fourniture des packages.  
 - Le mode *Non Embarqué* permet d'installer de nouveaux packages.  
 - Le mode *Embarqué* transfère les packages dans le projet.  
**↳** Basculer sur ce mode avant commit le projet sur un outil de versionnement.

### General

| Nom                  | Description                                           |
|----------------------|-------------------------------------------------------|
| Provider Port        | Défini le port utilisé par le serveur de registre     |
| Auto Update Registry | Active la mise à jour automatique du scoped registry  |
| Scope Accuracy       | Défini la profondeur des scopes créer automatiquement |

### Providers

Cette section liste les extensions et les packages qu'elles fournissent.  
Des fournisseurs peuvent être ajoutés en implémentant `IProvider`

### Rules

Cette section permet d'ajouter des règles permettant de modifier les informations d'un package à la volée.  
Une règle est composée de filtre(s) et d'action(s).  
Des filtres et des actions peuvent être ajoutés en implémentant respectivement `IFilter` et `IAction`  

### Advanced Settings

| Nom                       | Description                                                                        |
|---------------------------|------------------------------------------------------------------------------------|
| **Standalone Server**     | Permet de gérer un projet utilisable comme serveur indépendant                     |
| **↳** *Create*            | Créer le projet dans le sous dossier StandaloneRegistryServer                      |
| **↳** *Clean*             | Nettoie le projet dans le sous dossier StandaloneRegistryServer                    |
| **↳** *Delete*            | Supprime le projet dans le sous dossier StandaloneRegistryServer                   |
| **Clear Cache**           | Permet de nettoyer les caches des packages                                         |
| **↳** *Unity*             | Nettoie le cache [Global](https://docs.unity3d.com/Manual/upm-cache.html) & Projet |
| **↳** *Local Server*      | Nettoie le cache des fournisseurs                                                  |
| **↳** *Both*              | Nettoie les deux caches ci-dessus                                                  |
| **↳** *□ Save force sync* | Enregistre le nettoyage de cache pour le répercuter aux autres développeurs        |
| Debug Mode                | Active les logs complets et ajoute des informations aux packages                   |

## Safe Mode

Si Unity démarre en safe mode le serveur de fournisseurs ne pourra pas se lancer. Pour cela, vous pouvez lancer l'extension au sein d'un projet dédié.  
1. Vous pouvez créer ce projet en utilisant le bouton `Create` dans les `Advanced Settings` de **LFusion Local Registry Provider**.
2. Vous pouvez aussi le créer manuellement.
	- Copier les dossiers Packages et ProjectSettings du projet dans un nouveau dossier.
	- Créer un nouveau dossier `Assets` vide dans le dossier créer précédemment.

## LFusion Extensions

| Name                       | Package Name                                     | Description                                                                                                  |
|----------------------------|--------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Tgz Provider               | com.lfusion.localregistryprovider.tgzprovider    | Référence des packages locaux ou en ligne. Sa classe `ITgzProvider` simplifie le développement d'extensions. |
| Google Provider            | com.lfusion.localregistryprovider.googleprovider | Référence les packages Google hébergés sur : https://developers.google.com/unity/archive et Github.          |
| ~~Unity Package Provider~~ | ~~com.lfusion.localregistryprovider.xxxx~~       | ~~Converti et référence des packages depuis des .unitypackage~~                                              |
