﻿# LFusion Local Registry Provider

This package is used to create a registry provider linked to the project.  
Packages can be added by extensions implementing a provider.

 1. [Prerequisites](#prerequisites)
 2. [Installation](#installation)
 3. [Configuration](#configuration)
 4. [Safe Mode](#safe-mode)
 5. [LFusion Extensions](#lfusion-extensions)

## Prerequisites

 - **Unity 2020** or higher

## Installation

### Install via Gitlab registry

 - Add the [LFusion Upm-Registry](https://gitlab.com/lfusion/upm-registry)
 - **For Unity 2021 or highter**
	 - Package Manager
	 - **\+** Add package by name...
		 - `Name : com.lfusion.localregistryprovider.core`
 - **For all supported Unity versions**
	 - Open `manifest.json` in the Packages folder of the project.
	 - Add `"com.lfusion.localregistryprovider.core": "x.x.x",` to the `dependencies` block, replacing `x.x.x` with the desired version.
	 - Example for version `1.0.0` :
```json
{
  "dependencies": {
    "com.lfusion.localregistryprovider.core": "1.0.0",
    [...]
  },
  [...]
}
```

### Install via tarball

 - Download `com.lfusion.localregistryprovider.core-x.x.x.tgz` from [releases](https://gitlab.com/lfusion/localregistryprovider/core/-/releases).
 - Package Manager
 - **\+** Add package from tarball...
 - Select  `com.lfusion.localregistryprovider.core-x.x.x.tgz`

## Configuration

LFusion Local Registry Provider settings can be accessed from:   

 1. `Edit/Project Settings...`
 2. `Package Manager/Local Registry`

### Package Mode

Indicates the package delivery mode, *Disembedded* (local server) or *Embedded*.  
Switches package delivery mode.  
 - *Disembedded* mode is used to install new packages.  
 - *Embedded* mode transfers packages to the project.  
**↳** Switch to this mode before committing the project with a versioning tool.  

### General

| Name                 | Description                                           |
|----------------------|-------------------------------------------------------|
| Provider Port        | Defines the port used by the registry server          |
| Auto Update Registry | Enables automatic updating of the scoped registry     |
| Scope Accuracy       | Defines the depth of automatically created scopes     |

### Providers

This section lists the extensions and the packages they provide.  
Providers can be added by implementing `IProvider`.

### Rules

This section allows you to add rules for modifying package information on the fly.  
A rule is made up of filter(s) and action(s).  
Filters and actions can be added by implementing `IFilter` and `IAction` respectively.  

### Advanced Settings

| Nom                       | Description                                                                        |
|---------------------------|------------------------------------------------------------------------------------|
| **Standalone Server**     | Allows you to manage a project that can be used as an independent server           |
| **↳** *Create*            | Create the project in the StandaloneRegistryServer subfolder                       |
| **↳** *Clean*             | Cleans up the project in the StandaloneRegistryServer subfolder                    |
| **↳** *Delete*            | Deletes the project from the StandaloneRegistryServer subfolder                    |
| **Clear Cache**           | Allows you to clean package caches                                                 |
| **↳** *Unity*             | Clean cache [Global](https://docs.unity3d.com/Manual/upm-cache.html) & Project     |
| **↳** *Local Server*      | Clean providers cache                                                              |
| **↳** *Both*              | Clean the two caches above                                                         |
| **↳** *□ Save force sync* | Saves cache clean-up to be passed on to other developers                           |
| Debug Mode                | Activate full logs and add information to packages                                 |

## Safe Mode

If Unity starts up in safe mode, the provider server will not be able to launch. To do this, you can launch the extension within a dedicated project.  
1. You can create this project using the `Create` button in the `Advanced Settings` of **LFusion Local Registry Provider**.
2. You can also create it manually.
	- Copy the Packages and ProjectSettings folders from the project into a new folder.
	- Create a new empty `Assets` folder in the previously created folder.  

## LFusion Extensions

| Name                       | Package Name                                     | Description                                                                                                  |
|----------------------------|--------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Tgz Provider               | com.lfusion.localregistryprovider.tgzprovider    | Reference local or online packages. Its `ITgzProvider` class simplifies the development of extensions.       |
| Google Provider            | com.lfusion.localregistryprovider.googleprovider | Reference Google packages hosted on: https://developers.google.com/unity/archive and Github.                 |
| ~~Unity Package Provider~~ | ~~com.lfusion.localregistryprovider.xxxx~~       | ~~Convert and reference packages from .unitypackages~~                                                       |