/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using UnityEditor;
	using Upm = UnityEditor.PackageManager;

	[InitializeOnLoad]
	public static class EmbeddingManager
	{
		private const string LOG_PREFIX = "<color=orange>[EmbeddingManaged]</color>";

		public static EmbedState EmbedState { get => Settings.instance.EmbedState; }

		static EmbeddingManager()
		{
			switch (Settings.instance.EmbedState)
			{
				case EmbedState.Disembedded:
					break;
				case EmbedState.Embedding:
					Embed();
					break;
				case EmbedState.Embedded:
					break;
				case EmbedState.Disembedding:
					Disembed();
					break;
				default:
					break;
			}
		}

		public static async void Embed()
		{
			if (EmbedState == EmbedState.Disembedded || EmbedState == EmbedState.Embedding)
			{
				Settings.instance.SetEmbedState(EmbedState.Embedding);

				try
				{
					Upm.PackageInfo[] packageToEmbed = await Helpers.GetDisembeddedPackageInfosAsync();

					string log = "Packages remaining to embed: ";
					for (int i = 0; i < packageToEmbed.Length; i++)
					{
						log += "\n" + packageToEmbed[i].name;
					}
					Log(log);

					if (packageToEmbed.Length > 0)
					{
						await Helpers.RequestEmbedPackagesAsync(packageToEmbed);

						Upm.PackageInfo[] packageRemainingToEmbed = await Helpers.GetDisembeddedPackageInfosAsync();

						if (packageRemainingToEmbed.Length == 0)
						{
							Log("All packages from Local Registry was embedded");
							Settings.instance.SetEmbedState(EmbedState.Embedded);
						}
					}
					else
					{
						Log("All packages from Local Registry was embedded");
						Settings.instance.SetEmbedState(EmbedState.Embedded);
					}

				}
				catch (System.Exception)
				{
					Settings.instance.SetEmbedState(EmbedState.Disembedded);
					throw;
				}
			}
		}

		public static async void Disembed()
		{
			if (EmbedState == EmbedState.Embedded || EmbedState == EmbedState.Disembedding)
			{
				Settings.instance.SetEmbedState(EmbedState.Disembedding);

				try
				{
					string[] packageNamesToDisembed = await Helpers.GetEmbeddedPackageNamesAsync();

					if (packageNamesToDisembed.Length > 0)
					{
						await Helpers.DisembedPackagesAsync(packageNamesToDisembed);
					}

					Settings.instance.SetEmbedState(EmbedState.Disembedded);
				}
				catch (System.Exception)
				{
					Settings.instance.SetEmbedState(EmbedState.Embedded);
					throw;
				}
			}
		}

#pragma warning disable IDE0051 // Supprimer les membres priv�s non utilis�s
		private static void Log(string log)
		{
			Debug.Log(log, LOG_PREFIX);
		}

		private static void LogWarning(string log)
		{
			Debug.LogWarning(log, LOG_PREFIX);
		}

		private static void LogError(string log)
		{
			Debug.LogError(log, LOG_PREFIX);
		}
#pragma warning restore IDE0051 // Supprimer les membres priv�s non utilis�s
	}
}