/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using Upm = UnityEditor.PackageManager;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Threading.Tasks;
	using UnityEditor;

	[InitializeOnLoad]
	public static class CacheManager
	{

		#region Constantes
		private const string LOG_PREFIX = "<color=orange>[Core-CacheManager]</color>";
		private const string SAVE_RELATIVE_PATH = "Library/LFusion/LocalRegisteryProvider/LocalRegistryProviderCacheInfo.json";

		private const string RELATIVE_PACKAGECACHE_PATH = "Library/PackageCache";

		private const string UPM_KEY = "LastUpmCacheClear";
		private const string LOCALSERVER_KEY = "LastLocalServerCacheClear";
		#endregion Constantes

		#region Fields
		private static readonly string sk_cacheInfoPath;
		private static readonly string sk_projectPackageCachePath;

		private static readonly Dictionary<string, ClearCacheRef> sk_cache = new Dictionary<string, ClearCacheRef>()
		{
			{ UPM_KEY, new ClearCacheRef(0, new DateTime(0)) },
			{ LOCALSERVER_KEY, new ClearCacheRef(0, new DateTime(0)) }
		};

		private static bool s_clearIsRunning = false;
		#endregion Fields

		#region Properties
		private static string CacheProviderRef { get => "localhost_" + Settings.instance.ServerPort; }
		public static bool ClearIsRunning { get => s_clearIsRunning; }
		#endregion Properties

		#region Events
		private static Action s_clearStarted = null;
		public static event Action ClearStarted
		{
			add
			{
				s_clearStarted -= value;
				s_clearStarted += value;
			}
			remove
			{
				s_clearStarted -= value;
			}
		}

		private static Action s_clearEnded = null;
		public static event Action ClearEnded
		{
			add
			{
				s_clearEnded -= value;
				s_clearEnded += value;
			}
			remove
			{
				s_clearEnded -= value;
			}
		}
		#endregion Events

		#region Methods
		#region Constructor
		static CacheManager()
		{
			sk_cacheInfoPath = string.Format("{0}/{1}", Helpers.ProjectPath, SAVE_RELATIVE_PATH);
			sk_projectPackageCachePath = string.Format("{0}/{1}", Helpers.ProjectPath, RELATIVE_PACKAGECACHE_PATH);

			LoadCache();

			if (sk_cache.ContainsKey(LOCALSERVER_KEY))
			{
				ClearCacheRef localRef = sk_cache[LOCALSERVER_KEY];
				ClearCacheRef projectRef = Settings.instance.LastLocalServerCacheClear;

				if (localRef.ID < projectRef.ID || localRef.GetDateTime() < projectRef.GetDateTime())
				{
					Clear(PackageCacheType.LocalServer);
				}

				sk_cache[LOCALSERVER_KEY] = projectRef;
			}
			else
			{
				Clear(PackageCacheType.LocalServer);
				sk_cache.Add(LOCALSERVER_KEY, Settings.instance.LastLocalServerCacheClear);
			}

			if (sk_cache.ContainsKey(UPM_KEY))
			{
				ClearCacheRef localRef = sk_cache[UPM_KEY];
				ClearCacheRef projectRef = Settings.instance.LastUpmCacheClear;

				if (localRef.ID < projectRef.ID || localRef.GetDateTime() < projectRef.GetDateTime())
				{
					Clear(PackageCacheType.Upm);
				}

				sk_cache[UPM_KEY] = projectRef;
			}
			else
			{
				Clear(PackageCacheType.Upm);
				sk_cache.Add(UPM_KEY, Settings.instance.LastUpmCacheClear);
			}

			SaveCache();
		}
		#endregion Constructor

		#region Public
		public static void Clear(PackageCacheType cacheType, bool save = false)
		{
			if (s_clearIsRunning)
			{
				return;
			}

			if (EditorUtility.DisplayDialog("Clear cache?", string.Format("{0} chache will be cleared.\nAre you sure?", cacheType.ToString()), "Ok", "Cancel") == false)
			{
				Log(string.Format("Clear {0} cache was canceled", cacheType.ToString()));
				return;
			}

			s_clearIsRunning = true;
			s_clearStarted?.Invoke();

			if (cacheType.HasFlag(PackageCacheType.LocalServer))
			{
				ProviderManager.RefreshAll(true);
				Log(string.Format("{0} cache was refreshed", PackageCacheType.LocalServer.ToString()));

				if (save)
				{
					ClearCacheRef localServerCacheRef = Settings.instance.LastLocalServerCacheClear;
					localServerCacheRef.Increment();

					if (sk_cache.ContainsKey(LOCALSERVER_KEY) == false)
					{
						sk_cache.Add(LOCALSERVER_KEY, localServerCacheRef);
					}
					else
					{
						sk_cache[LOCALSERVER_KEY] = localServerCacheRef;
					}

					Settings.instance.LastLocalServerCacheClear = localServerCacheRef;
				}
			}
			if (cacheType.HasFlag(PackageCacheType.Upm))
			{
				ClearUpmCache(CacheProviderRef);
				ClearProjectUpmCache();

				Log(string.Format("{0} cache was cleared", PackageCacheType.Upm.ToString()));

				Upm.Client.Resolve();

				if (save)
				{
					ClearCacheRef upmCacheRef = Settings.instance.LastUpmCacheClear;
					upmCacheRef.Increment();

					if (sk_cache.ContainsKey(UPM_KEY) == false)
					{
						sk_cache.Add(UPM_KEY, upmCacheRef);
					}
					else
					{
						sk_cache[UPM_KEY] = upmCacheRef;
					}

					Settings.instance.LastUpmCacheClear = upmCacheRef;
				}
			}

			if (save)
			{
				SaveCache();
			}

			s_clearIsRunning = false;
			s_clearEnded?.Invoke();
		}

		public static async Task ClearAsync(PackageCacheType cacheType, bool save = false)
		{
			if (s_clearIsRunning)
			{
				return;
			}

			s_clearIsRunning = true;
			s_clearStarted?.Invoke();

			if (cacheType.HasFlag(PackageCacheType.LocalServer))
			{
				await ProviderManager.RefreshAllAsync(true);
				Log(string.Format("{0} cache was refreshed", PackageCacheType.LocalServer.ToString()));

				if (save)
				{
					ClearCacheRef localServerCacheRef = Settings.instance.LastLocalServerCacheClear;
					localServerCacheRef.Increment();

					if (sk_cache.ContainsKey(LOCALSERVER_KEY) == false)
					{
						sk_cache.Add(LOCALSERVER_KEY, localServerCacheRef);
					}
					else
					{
						sk_cache[LOCALSERVER_KEY] = localServerCacheRef;
					}

					Settings.instance.LastLocalServerCacheClear = localServerCacheRef;
				}
			}

			if (cacheType.HasFlag(PackageCacheType.Upm))
			{
				ClearUpmCache(CacheProviderRef);
				ClearProjectUpmCache();

				Log(string.Format("{0} cache was cleared", PackageCacheType.Upm.ToString()));

				Upm.Client.Resolve();

				if (save)
				{
					ClearCacheRef upmCacheRef = Settings.instance.LastUpmCacheClear;
					upmCacheRef.Increment();

					if (sk_cache.ContainsKey(UPM_KEY) == false)
					{
						sk_cache.Add(UPM_KEY, upmCacheRef);
					}
					else
					{
						sk_cache[UPM_KEY] = upmCacheRef;
					}

					Settings.instance.LastUpmCacheClear = upmCacheRef;
				}
			}

			if (save)
			{
				SaveCache();
			}
			s_clearIsRunning = false;
			s_clearEnded?.Invoke();
		}
		#endregion Public

		#region Private
		private static void LoadCache()
		{
			if (File.Exists(sk_cacheInfoPath))
			{
				string json = File.ReadAllText(sk_cacheInfoPath);

				sk_cache.ParseJson(json);
			}
		}

		private static void SaveCache()
		{
			Directory.CreateDirectory(Directory.GetParent(sk_cacheInfoPath).FullName);

			string json = sk_cache.ToJson();

			File.WriteAllText(sk_cacheInfoPath, json);
		}

		private static void ClearUpmCache(string provider)
		{
			string cachePath =
#if UNITY_EDITOR_WIN
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Unity", "cache")
#elif UNITY_EDITOR_OSX
				"$HOME/Library/Unity/cache"
#elif UNITY_EDITOR_LINUX
				"$HOME/.config/unity3d/cache"
#endif
				;

			string npmCachePath = Path.Combine(cachePath, "npm", provider);
			string packagesCachePath = Path.Combine(cachePath, "packages", provider);

			int npmCachePackagesCount = 0;
			int npmCacheSubDirPackagesCount = 0;
			int packagesCachePackagesCount = 0;

			if (Directory.Exists(npmCachePath))
			{
				foreach (string npmCachePathItem in Directory.EnumerateDirectories(npmCachePath))
				{
					if (npmCachePathItem.EndsWith(provider))
					{
						foreach (string npmCacheSubDirPathItem in Directory.EnumerateDirectories(npmCachePathItem))
						{
							if (DeleteDirectoryWithExceptionCatch(npmCacheSubDirPathItem, true))
							{
								npmCacheSubDirPackagesCount++;
							}
						}
						continue;
					}

					if (DeleteDirectoryWithExceptionCatch(npmCachePathItem, true))
					{
						npmCachePackagesCount++;
					}
				}
			}

			if (Directory.Exists(packagesCachePath))
			{
				foreach (string packagesCachePathItem in Directory.EnumerateDirectories(packagesCachePath))
				{
					if (DeleteDirectoryWithExceptionCatch(packagesCachePathItem, true))
					{
						packagesCachePackagesCount++;
					}
				}
			}

			if (npmCachePackagesCount == npmCacheSubDirPackagesCount && npmCacheSubDirPackagesCount == packagesCachePackagesCount)
			{
				Log(string.Format("{0} packages was cleared from global cache", npmCachePackagesCount));
			}
			else
			{
				int fullClear = Math.Min(npmCachePackagesCount, Math.Min(npmCacheSubDirPackagesCount, packagesCachePackagesCount));
				int partialClear = Math.Max(npmCachePackagesCount, Math.Max(npmCacheSubDirPackagesCount, packagesCachePackagesCount));

				Log(string.Format("Some packages was not fully cleared from global cache\nFull: {0}\nPartial: {1}", fullClear, partialClear));
			}
		}

		private static void ClearProjectUpmCache()
		{
			IDictionary<string, PackageInfo> packages = ProviderManager.GetPackages();

			int clearedPackagesCount = 0;

			foreach (var packageInfo in packages)
			{
				foreach (var packageVersionInfo in packageInfo.Value.Versions)
				{
					if (TryClearProjectPackageCache(packageVersionInfo.Value))
					{
						clearedPackagesCount++;
					}
				}
			}

			Log(string.Format("{0} packages was cleared from project cache", clearedPackagesCount));
		}

		private static bool TryClearProjectPackageCache(PackageVersionInfo packageVersionInfo)
		{
			string packageCachePath = Path.Combine(sk_projectPackageCachePath, string.Format("{0}@{1}", packageVersionInfo.Name, packageVersionInfo.Version));

			if (Directory.Exists(packageCachePath))
			{
				return DeleteDirectoryWithExceptionCatch(packageCachePath, true);
			}

			return false;
		}

		private static bool DeleteDirectoryWithExceptionCatch(string path, bool recursive = false)
		{
			try
			{
				Directory.Delete(path, recursive);
				return true;
			}
			catch (Exception e)
			{
				LogError(string.Format("Deleting {0} directory failed\nError: {1}", path, e.Message));
			}
			return false;
		}
		#endregion Private

		#region Debugs

		private static void Log(string log)
		{
			Debug.Log(log, LOG_PREFIX);
		}

		private static void LogWarning(string log)
		{
			Debug.LogWarning(log, LOG_PREFIX);
		}

		private static void LogError(string log)
		{
			Debug.LogError(log, LOG_PREFIX);
		}

		#endregion Debugs
		#endregion Methods
	}
}