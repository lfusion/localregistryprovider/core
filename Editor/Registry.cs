/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using UnityEditor;
	using UnityEditor.PackageManager;

	public static class Registry
	{
		#region Constants
		private const string LOG_PREFIX = "<color=orange>[Registry]</color>";
		#endregion Constants

		#region Fields
		private static bool s_updateScopedRegistryIsRunning = false;
		#endregion Fields

		#region Properties
		public static bool UpdateScopedRegistryIsRunning { get => s_updateScopedRegistryIsRunning; }
		#endregion Properties

		#region Events
		private static Action s_updateScopedRegistryStarted = null;
		public static event Action UpdateScopedRegistryStarted
		{
			add
			{
				s_updateScopedRegistryStarted -= value;
				s_updateScopedRegistryStarted += value;
			}
			remove
			{
				s_updateScopedRegistryStarted -= value;
			}
		}

		private static Action s_updateScopedRegistryEnded = null;
		public static event Action UpdateScopedRegistryEnded
		{
			add
			{
				s_updateScopedRegistryEnded -= value;
				s_updateScopedRegistryEnded += value;
			}
			remove
			{
				s_updateScopedRegistryEnded -= value;
			}
		}
		#endregion Events

		#region Methods
		[InitializeOnLoadMethod]
		public static async Task UpdateScopedRegistryAsync()
		{
			if (Settings.instance.AutoUpdateRegistry == false || s_updateScopedRegistryIsRunning)
			{
				return;
			}

			s_updateScopedRegistryIsRunning = true;
			s_updateScopedRegistryStarted?.Invoke();
			try
			{
				await UpdateScopedRegistryInternalAsync();
			}
			catch (Exception e)
			{
				Debug.LogError("Updating Scoped Registry failed: " + e.Message + "\n" + e.StackTrace, LOG_PREFIX);
			}
			s_updateScopedRegistryIsRunning = false;
			s_updateScopedRegistryEnded?.Invoke();
		}

		private static async Task UpdateScopedRegistryInternalAsync()
		{
			//Check if registry is already created

			RegistryInfo[] registries = await InternalHelpers.GetRegistriesAsync();
			bool registryAlreadyCreated = false;

			for (int i = 0; i < registries.Length; i++)
			{
				if (registries[i].name == Constants.RegistryName)
				{
					registryAlreadyCreated = true;
					break;
				}
			}

			//List scopes

			List<string> scopes = new List<string>();
			IDictionary<string, PackageInfo> packages = ProviderManager.GetPackages();

			foreach (var package in packages)
			{
				List<string> splitedScope = new List<string>(package.Key.Split('.'));
				while (splitedScope.Count > Settings.instance.ScopeAccuracy)
				{
					splitedScope.RemoveAt(splitedScope.Count - 1);
				}

				for (int i = 1; i < splitedScope.Count; i++)
				{
					splitedScope[i] = "." + splitedScope[i];
				}

				string scope = string.Concat(splitedScope);

				if (scopes.Contains(scope) == false)
				{
					scopes.Add(scope);
				}
			}

			//Set Registry

			if (registryAlreadyCreated)
			{
				await InternalHelpers.UpdateScopedRegistryAsync(Constants.RegistryName, Constants.RegistryName, Server.Uri, scopes.ToArray());
			}
			else
			{
				await InternalHelpers.AddScopedRegistryAsync(Constants.RegistryName, Server.Uri, scopes.ToArray());
			}

			//Force Update
			await InternalHelpers.GetRegistriesAsync();

			Debug.Log("Local Provider Scoped Registry updated", LOG_PREFIX);
		}
		#endregion Methods
	}
}
