/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System.Collections.Generic;
	using System.Text;
	using UnityEngine;
	using System.IO;
	using System.Linq;
	using System.Security.Cryptography;
	using System;
	using UnityEditor;
	using Upm = UnityEditor.PackageManager;
	using System.Threading.Tasks;
	using System.Reflection;

	[InitializeOnLoad]
	public static class Helpers
	{
		#region Constants
		private const string STANDALONE_RELATIVE_PATH = "StandaloneRegistryServer";

		private static readonly string[] DIR_TO_COPY =
		{
			"Packages",
			"ProjectSettings"
		};
		#endregion Constants

		#region Fields
		private static readonly string sk_projectPath;
		#endregion Fields

		#region Properties
		public static string ProjectPath { get => sk_projectPath; }
		#endregion

		#region Methods
		static Helpers()
		{
			sk_projectPath = Application.dataPath.Replace("/Assets", "");
		}

		#region Json

		#region Extensions
		public static string ToJson<T>(this IDictionary<string, T> dictionary) where T : IToJson
		{
			StringBuilder jsonBuilder = new StringBuilder("{");

			int dicCount = dictionary.Count;

			foreach (var item in dictionary)
			{
				Append(jsonBuilder, item.Key, item.Value.ToJson(), false);
				jsonBuilder.Append(",");
			}

			if (dicCount > 0)
			{
				jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
			}

			jsonBuilder.Append("}");

			return jsonBuilder.ToString();
		}

		public static string ToJson(this IDictionary<string, string> dictionary)
		{
			StringBuilder jsonBuilder = new StringBuilder("{");

			int dicCount = dictionary.Count;

			foreach (var item in dictionary)
			{
				Append(jsonBuilder, item.Key, item.Value.LineBreakToJson(), true);
				jsonBuilder.Append(",");
			}

			if (dicCount > 0)
			{
				jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
			}

			jsonBuilder.Append("}");

			return jsonBuilder.ToString();
		}

		public static string ToJson<T>(this IList<T> list) where T : IToJson
		{
			StringBuilder jsonBuilder = new StringBuilder("[");

			int listCount = list.Count;

			foreach (var item in list)
			{
				jsonBuilder.Append(item.ToJson());
				jsonBuilder.Append(",");
			}

			if (listCount > 0)
			{
				jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
			}

			jsonBuilder.Append("]");

			return jsonBuilder.ToString();
		}

		public static void ParseJson<T>(this IDictionary<string, T> dictionary, string json) where T : IParseJson, new()
		{
			int currentIndex = 0;

			while (currentIndex + 1 < json.Length)
			{
				currentIndex++;

				if (json[currentIndex] == '"')
				{
					string key;
					int keyStart = currentIndex;
					while (true)
					{
						currentIndex++;

						if (json[currentIndex] == '"')
						{
							currentIndex++;
							key = json.Substring(keyStart, currentIndex - keyStart).Replace("\"", "");
							break;
						}
					}

					T value = new T();

					string valueJson = GetJsonPart(json, currentIndex);

					value.ParseJson(valueJson);

					currentIndex += valueJson.Length;

					if (dictionary.ContainsKey(key))
					{
						dictionary[key] = value;
					}
					else
					{
						dictionary.Add(key, value);
					}
				}
			}
		}

		public static void ParseJson(this IDictionary<string, string> dictionary, string json)
		{
			int currentIndex = 0;

			while (currentIndex + 1 < json.Length)
			{
				currentIndex++;

				if (json[currentIndex] == '"')
				{
					string key;
					int keyStart = currentIndex;
					while (true)
					{
						currentIndex++;

						if (json[currentIndex] == '"')
						{
							key = json.Substring(keyStart, currentIndex - keyStart + 1).Replace("\"", "");
							break;
						}
					}

					while (true)
					{
						currentIndex++;

						if (json[currentIndex] == '"')
						{
							string value;
							int valueStart = currentIndex;

							while (true)
							{
								currentIndex++;

								if (json[currentIndex] == '"')
								{
									value = json.Substring(valueStart, currentIndex - valueStart + 1).Replace("\"", "").LineBreakParseJson();
									break;
								}
							}

							if (dictionary.ContainsKey(key))
							{
								dictionary[key] = value;
							}
							else
							{
								dictionary.Add(key, value);
							}
							break;
						}
					}
				}
			}
		}

		public static void ParseJson<T>(this IList<T> list, string json) where T : IParseJson, new()
		{
			list.Clear();
			int currentIndex = 0;

			while (currentIndex + 1 < json.Length)
			{
				currentIndex++;
				if (json[currentIndex] == '{')
				{
					string itemJson = GetJsonPart(json, currentIndex);

					T item = new T();
					item.ParseJson(itemJson);

					list.Add(item);

					currentIndex += itemJson.Length;
				}
			}
		}

		public static string LineBreakToJson(this string str)
		{
			return str?.Replace("\n", "\\n");
		}

		public static string LineBreakParseJson(this string str)
		{
			return str?.Replace("\\n", "\n");
		}
		#endregion Extensions

		public static string GetJsonPart(string json, int startIndex)
		{
			int currentPos = startIndex;

			while (true)
			{
				if (json[currentPos] == '{')
				{
					startIndex = currentPos;
					break;
				}
				currentPos++;
			}

			int level = 1;

			while (level > 0)
			{
				currentPos++;

				switch (json[currentPos])
				{
					case '{':
						level++;
						break;
					case '}':
						level--;
						break;
				}
			}

			currentPos++;

			return json.Substring(startIndex, currentPos - startIndex);
		}

		public static string KeyToJson(string key)
		{
			return string.Format("\"{0}\":", key);
		}

		public static void Append(StringBuilder stringBuilder, string key, string value, bool quote)
		{
			if (quote)
			{
				stringBuilder.AppendFormat("\"{0}\":\"{1}\"", key, value);
			}
			else
			{
				stringBuilder.AppendFormat("\"{0}\":{1}", key, value);
			}
		}

		public static StringBuilder PretifyJson(StringBuilder stringBuilder)
		{
			int index = 0;

			int indentLevel = 0;

			bool isUnderQuote = false;

			while (index < stringBuilder.Length)
			{
				char currentChar = stringBuilder[index];

				if(currentChar == '"')
				{
					isUnderQuote = !isUnderQuote;
				}

				if(isUnderQuote == false)
				{
					if (currentChar == '{' || currentChar == '[' || currentChar == ',')
					{
						if(currentChar != ',')
						{
							if(index > 0)
							{
								stringBuilder.Insert(index, '\n');
								index++;
								IndentOf(stringBuilder, index, indentLevel);
								index += indentLevel;
							}
							indentLevel++;
						}

						index++;
						stringBuilder.Insert(index, '\n');
						index++;
						IndentOf(stringBuilder, index, indentLevel);
						index += indentLevel - 1;
					}
					else if (currentChar == '}' || currentChar == ']')
					{
						stringBuilder.Insert(index, '\n');
						indentLevel--;
						index++;
						IndentOf(stringBuilder, index, indentLevel);
						index += indentLevel;
					}
				}

				index++;
			}

			return stringBuilder;
		}

		private static void IndentOf(StringBuilder stringBuilder, int index, int indentLevel)
		{
			for (int i = 0; i < indentLevel; i++)
			{
				stringBuilder.Insert(index, '\t');
			}
		}

		#endregion Json

		#region Packages
		public static string GetInstalledPackageVersion(string packageName)
		{
			string packagePath = "Packages/" + packageName;
			if (AssetDatabase.IsValidFolder(packagePath))
			{
				TextAsset packageInfo = AssetDatabase.LoadAssetAtPath<TextAsset>(packagePath + "/package.json");
				PackageVersionInfo packageVersionInfo = new PackageVersionInfo();
				packageVersionInfo.ParseJson(packageInfo.text);
				return packageVersionInfo.Version;
			}
			return null;
		}

		public static string RegistryDateTimeFormat(DateTime dateTime)
		{
			return dateTime.ToString("yyyy'-'MM'-'dd'T'hh:mm:ss.FFF'Z'");
		}

		public static string ComputeShasum(string filePath)
		{
			DirectoryInfo directory = Directory.GetParent(filePath);

			if (directory.Exists == false || File.Exists(filePath) == false)
			{
				return null;
			}

			using (var stream = File.OpenRead(filePath))
			{
				return ComputeShasum(stream);
			}
		}

		public static string ComputeShasum(byte[] data)
		{
			using (var stream = new MemoryStream(data))
			{
				return ComputeShasum(stream);
			}
		}

		public static string ComputeShasum(Stream stream)
		{
			SHA1CryptoServiceProvider cryptoServiceProvider = new SHA1CryptoServiceProvider();

			byte[] hash = cryptoServiceProvider.ComputeHash(stream);

			return BitConverter.ToString(hash).Replace("-", "").ToLower();
		}

		public static async Task RequestEmbedPackagesAsync(params Upm.PackageInfo[] packageInfos)
		{
			foreach (Upm.PackageInfo packageInfo in packageInfos)
			{
				if (packageInfo.source != Upm.PackageSource.Embedded && packageInfo.source != Upm.PackageSource.BuiltIn)
				{
					await CopyDirectoryFromShellAsync(packageInfo.resolvedPath, Path.Combine(ProjectPath, "Packages", packageInfo.name));
				}
			}

			Upm.Client.Resolve();
		}

		private static async Task CopyDirectoryFromShellAsync(string sourceDir, string destinationDir)
		{
			TaskCompletionSource<int> tsc = new TaskCompletionSource<int>();

			if (sourceDir.EndsWith("\\") == false)
			{
				sourceDir += "\\";
			}

			if (destinationDir.EndsWith("\\") == false)
			{
				destinationDir += "\\";
			}

#if UNITY_EDITOR_WIN
			string executeFileName = "xcopy";
			string executeArgs = string.Format("\"{0}\" \"{1}\" /s /y /r /h", sourceDir, destinationDir);
#elif UNITY_EDITOR_OSX
			string executeFileName = "cp";
			string executeArgs = string.Format("-R \"{0}\" \"{1}\"", sourceDir, destinationDir);
#elif UNITY_EDITOR_LINUX
			string executeFileName = "cp";
			string executeArgs = string.Format("-r \"{0}\" \"{1}\"", sourceDir, destinationDir);
#endif

			System.Diagnostics.Process process = new System.Diagnostics.Process();
			System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
			{
				WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
				FileName = executeFileName,
				Arguments = executeArgs
			};
			process.EnableRaisingEvents = true;
			process.StartInfo = startInfo;

			process.Exited += (sender, args) =>
			{
				tsc.SetResult(process.ExitCode);
				process.Dispose();
			};

			process.Start();

			await tsc.Task;
		}

		public static async Task DisembedPackagesAsync(params string[] packagesNames)
		{
			Upm.Requests.ListRequest listRequest = Upm.Client.List(true, true);

			while (listRequest.Status == Upm.StatusCode.InProgress)
			{
				await Task.Yield();
			}

			if (listRequest.Status == Upm.StatusCode.Success)
			{
				for (int i = 0; i < packagesNames.Length; i++)
				{

					Upm.PackageInfo packageInfo = listRequest.Result.SingleOrDefault(x => x.name == packagesNames[i]);

					if (packageInfo.source == Upm.PackageSource.Registry || FileUtil.DeleteFileOrDirectory(packageInfo.resolvedPath) == false)
					{
						Debug.LogError("Error");
					}
				}

				Upm.Client.Resolve();
			}
		}

		public static async Task<Upm.PackageInfo[]> GetDisembeddedPackageInfosAsync()
		{
			Upm.Requests.ListRequest listRequest = Upm.Client.List(true, true);
			while (listRequest.Status == Upm.StatusCode.InProgress)
			{
				await Task.Yield();
			}

			if (listRequest.Status == Upm.StatusCode.Success)
			{
				List<Upm.PackageInfo> packageNames = new List<Upm.PackageInfo>();

				IList<string> providerPackages = ProviderManager.GetPackages().Keys.ToList();

				for (int i = 0; i < providerPackages.Count; i++)
				{
					Upm.PackageInfo packageInfo = listRequest.Result.SingleOrDefault(x => x.name == providerPackages[i]);
					if (packageInfo != null && packageInfo.source == Upm.PackageSource.Registry)
					{
						packageNames.Add(packageInfo);
					}
				}

				return packageNames.ToArray();
			}
			else
			{
				throw new Exception("List Request to UPM failed: " + listRequest.Error);
			}
		}

		public static async Task<string[]> GetEmbeddedPackageNamesAsync()
		{
			Upm.Requests.ListRequest listRequest = Upm.Client.List(true, true);
			while (listRequest.Status == Upm.StatusCode.InProgress)
			{
				await Task.Yield();
			}

			if (listRequest.Status == Upm.StatusCode.Success)
			{

				List<string> packageNames = new List<string>();

				IList<string> providerPackages = ProviderManager.GetPackages().Keys.ToList();

				for (int i = 0; i < providerPackages.Count; i++)
				{
					Upm.PackageInfo packageInfo = listRequest.Result.SingleOrDefault(x => x.name == providerPackages[i]);
					if (packageInfo != null && packageInfo.source == Upm.PackageSource.Embedded)
					{
						packageNames.Add(packageInfo.name);
					}
				}

				return packageNames.ToArray();
			}
			else
			{
				throw new Exception("List Request to UPM failed: " + listRequest.Error);
			}
		}
		#endregion Packages

		#region SerializedProperty
		// https://github.com/lordofduct/spacepuppy-unity-framework/blob/master/SpacepuppyBaseEditor/EditorHelper.cs#L214

		/// <summary>
		/// Gets the object the property represents.
		/// </summary>
		/// <param name="prop"></param>
		/// <returns></returns>
		public static object GetTargetObjectOfProperty(SerializedProperty prop)
		{
			if (prop == null) return null;

			var path = prop.propertyPath.Replace(".Array.data[", "[");
			object obj = prop.serializedObject.targetObject;
			var elements = path.Split('.');
			foreach (var element in elements)
			{
				if (element.Contains("["))
				{
					var elementName = element.Substring(0, element.IndexOf("["));
					var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
					obj = GetValue_Imp(obj, elementName, index);
				}
				else
				{
					obj = GetValue_Imp(obj, element);
				}
			}
			return obj;
		}

		private static object GetValue_Imp(object source, string name, int index)
		{
			var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
			if (enumerable == null) return null;
			var enm = enumerable.GetEnumerator();

			for (int i = 0; i <= index; i++)
			{
				if (!enm.MoveNext()) return null;
			}
			return enm.Current;
		}

		private static object GetValue_Imp(object source, string name)
		{
			if (source == null)
				return null;
			var type = source.GetType();

			while (type != null)
			{
				var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				if (f != null)
					return f.GetValue(source);

				var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
				if (p != null)
					return p.GetValue(source, null);

				type = type.BaseType;
			}
			return null;
		}

		#endregion SerializedProperty

		#region StandaloneServerRegistry
		public static void CreateStandaloneServerProject()
		{
			foreach (string dir in DIR_TO_COPY)
			{
				string sourcePath = Path.Combine(ProjectPath, dir);
				string targetPath = Path.Combine(ProjectPath, STANDALONE_RELATIVE_PATH, dir);

				if (Directory.Exists(sourcePath))
				{
					CopyDirectory(sourcePath, targetPath, true);
				}
			}

			string assetsPath = Path.Combine(ProjectPath, STANDALONE_RELATIVE_PATH, "Assets");
			if (Directory.Exists(assetsPath) == false)
			{
				Directory.CreateDirectory(assetsPath);
			}

			string assetsFilePath = Path.Combine(assetsPath, "Assets~");
			if (File.Exists(assetsFilePath) == false)
			{
				File.Create(assetsFilePath).Dispose();
			}
		}

		public static void DeleteStandaloneServerProject()
		{
			string path = Path.Combine(ProjectPath, STANDALONE_RELATIVE_PATH);

			if (Directory.Exists(path))
			{
				Directory.Delete(Path.Combine(ProjectPath, STANDALONE_RELATIVE_PATH), true);
			}
		}

		public static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
		{
			// Get information about the source directory
			var dir = new DirectoryInfo(sourceDir);

			// Check if the source directory exists
			if (!dir.Exists)
				throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

			// Cache directories before we start copying
			DirectoryInfo[] dirs = dir.GetDirectories();

			// Create the destination directory
			Directory.CreateDirectory(destinationDir);

			// Get the files in the source directory and copy to the destination directory
			foreach (FileInfo file in dir.GetFiles())
			{
				string targetFilePath = Path.Combine(destinationDir, file.Name);
				file.CopyTo(targetFilePath);
			}

			// If recursive and copying subdirectories, recursively call this method
			if (recursive)
			{
				foreach (DirectoryInfo subDir in dirs)
				{
					string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
					CopyDirectory(subDir.FullName, newDestinationDir, true);
				}
			}
		}

		public static async Task CopyDirectoryAsync(string sourceDir, string destinationDir, bool recursive)
		{
			// Get information about the source directory
			var dir = new DirectoryInfo(sourceDir);

			// Check if the source directory exists
			if (!dir.Exists)
				throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

			// Cache directories before we start copying
			DirectoryInfo[] dirs = dir.GetDirectories();

			// Create the destination directory
			Directory.CreateDirectory(destinationDir);

			// Get the files in the source directory and copy to the destination directory
			foreach (FileInfo file in dir.GetFiles())
			{
				string targetFilePath = Path.Combine(destinationDir, file.Name);
				await CopyFileAsync(file.FullName, targetFilePath);
			}

			// If recursive and copying subdirectories, recursively call this method
			if (recursive)
			{
				foreach (DirectoryInfo subDir in dirs)
				{
					string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
					await CopyDirectoryAsync(subDir.FullName, newDestinationDir, true);
				}
			}
		}

		public static async Task CopyFileAsync(StreamReader Source, StreamWriter Destination)
		{
			char[] buffer = new char[0x1000];
			int numRead;
			while ((numRead = await Source.ReadAsync(buffer, 0, buffer.Length)) != 0)
			{
				await Destination.WriteAsync(buffer, 0, numRead);
			}
		}

		public static async Task CopyFileAsync(string sourceFilePath, string destinationFilePath)
		{
			using (StreamReader SourceReader = File.OpenText(sourceFilePath))
			{
				using (StreamWriter DestinationWriter = File.CreateText(destinationFilePath))
				{
					await CopyFileAsync(SourceReader, DestinationWriter);
				}
			}
		}
		#endregion StandaloneServerRegistry
		#endregion Methods
	}
}
