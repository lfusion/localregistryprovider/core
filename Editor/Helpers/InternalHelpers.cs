/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System.Reflection;
	using Upm = UnityEditor.PackageManager;
	using System;
	using System.Threading.Tasks;
	using UnityEditor.PackageManager;

	/// <summary>
	/// Internal Helpers.
	/// Allow access to some internal Package Manager Client methods
	/// </summary>
	public static class InternalHelpers
	{
		private static readonly MethodInfo sk_addScopedRegistry;
		private static readonly MethodInfo sk_updateScopedRegistry;
		private static readonly MethodInfo sk_removeScopedRegistry;
		private static readonly MethodInfo sk_getRegistries;

		private static readonly Type sk_updateScopedRegistryOptions;

		static InternalHelpers()
		{
			Type client = typeof(Upm.Client);

			var methods = client.GetMethods(BindingFlags.NonPublic | BindingFlags.Static);

			foreach (var method in methods)
			{
				if (method.Name == "AddScopedRegistry" && method.ReturnType.Name != "NativeStatusCode")
				{
					sk_addScopedRegistry = method;
				}

				if (method.Name == "UpdateScopedRegistry" && method.ReturnType.Name != "NativeStatusCode")
				{
					sk_updateScopedRegistry = method;
				}

				if (method.Name == "RemoveScopedRegistry" && method.ReturnType.Name != "NativeStatusCode")
				{
					sk_removeScopedRegistry = method;
				}

				if (method.Name == "GetRegistries" && method.ReturnType.Name != "NativeStatusCode")
				{
					sk_getRegistries = method;
				}
			}

			sk_updateScopedRegistryOptions = typeof(Upm.Client).Assembly.GetType("UnityEditor.PackageManager.UpdateScopedRegistryOptions");
		}

		/// <summary>
		/// Add Scoped Registry asynchronously
		/// </summary>
		/// <param name="registryName">registry Name</param>
		/// <param name="url">url</param>
		/// <param name="scopes">Array of scope</param>
		/// <returns>Async</returns>
		public static async Task AddScopedRegistryAsync(string registryName, string url, string[] scopes)
		{
			var req = sk_addScopedRegistry?.Invoke(null, new object[] { registryName, url, scopes });

			Type reqType = sk_addScopedRegistry.ReturnType;
			var isCompleted = reqType.GetProperty("IsCompleted");

			while (!(bool)isCompleted.GetValue(req))
			{
				await Task.Yield();
			}

		}

		/// <summary>
		/// Update Scoped Registry asynchronously
		/// </summary>
		/// <param name="registryName">Orginal Registry Name</param>
		/// <param name="newRegistryName">New Registry Name</param>
		/// <param name="url">New Registry Url</param>
		/// <param name="scopes">New array of scope</param>
		/// <returns>Async</returns>
		public static async Task UpdateScopedRegistryAsync(string registryName, string newRegistryName, string url, string[] scopes)
		{
			object updateScopedRegistryOptions = Activator.CreateInstance(sk_updateScopedRegistryOptions, BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { newRegistryName, url, scopes }, null);

			var req = sk_updateScopedRegistry?.Invoke(null, new object[] { registryName, updateScopedRegistryOptions });

			Type reqType = sk_updateScopedRegistry.ReturnType;
			var isCompleted = reqType.GetProperty("IsCompleted");

			while (!(bool)isCompleted.GetValue(req))
			{
				await Task.Yield();
			}
		}

		/// <summary>
		/// Remove Scoped Registry asynchronously
		/// </summary>
		/// <param name="registryName">Registry Name</param>
		/// <returns>Async</returns>
		public static async Task RemoveScopedRegistryAsync(string registryName)
		{
			var req = sk_removeScopedRegistry?.Invoke(null, new object[] { registryName });

			Type reqType = sk_removeScopedRegistry.ReturnType;
			var isCompleted = reqType.GetProperty("IsCompleted");

			while (!(bool)isCompleted.GetValue(req))
			{
				await Task.Yield();
			}
		}

		/// <summary>
		/// Get Scoped Registries asynchronously
		/// Can be used to force unity to refresh Scoped Registry
		/// </summary>
		/// <returns>Async Registry info Array</returns>
		public static async Task<RegistryInfo[]> GetRegistriesAsync()
		{
			var req = sk_getRegistries?.Invoke(null, null);

			Type reqType = sk_getRegistries.ReturnType;
			var isCompletedProperty = reqType.GetProperty("IsCompleted");

			while (!(bool)isCompletedProperty.GetValue(req))
			{
				await Task.Yield();
			}

			var resultProperty = reqType.GetProperty("Result");
			RegistryInfo[] result = (RegistryInfo[])resultProperty.GetValue(req);

			return result;
		}
	}
}