﻿/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	/// <summary>
	/// Some Debug Helpers
	/// </summary>
	public static class Debug
	{
		#region Contants

		private const string k_prefix = "<b><color=#0092C4>◀LFusion◆Local Registry Provider▶</color></b>";

		#endregion Contants

		#region Methods

		public static void Log(string log, string prefix = null)
		{
			if (Settings.instance.DebugMode == false)
			{
				return;
			}

			if (prefix != null)
			{
				UnityEngine.Debug.LogFormat("{0}{1} {2}", k_prefix, prefix, log);
			}
			else
			{
				UnityEngine.Debug.LogFormat("{0} {1}", k_prefix, log);
			}
		}

		public static void LogWarning(string log, string prefix = null)
		{
			if (Settings.instance.DebugMode == false)
			{
				return;
			}

			if (prefix != null)
			{
				UnityEngine.Debug.LogWarningFormat("{0}{1} {2}", k_prefix, prefix, log);
			}
			else
			{
				UnityEngine.Debug.LogWarningFormat("{0} {1}", k_prefix, log);
			}
		}

		public static void LogError(string log, string prefix = null)
		{
			//if (Settings.instance.DebugMode == false)
			//{
			//	return;
			//}

			if (prefix != null)
			{
				UnityEngine.Debug.LogErrorFormat("{0}{1} {2}", k_prefix, prefix, log);
			}
			else
			{
				UnityEngine.Debug.LogErrorFormat("{0} {1}", k_prefix, log);
			}
		}

		#endregion Methods
	}
}