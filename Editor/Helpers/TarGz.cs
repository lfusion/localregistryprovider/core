// based on : https://gist.github.com/Su-s/438be493ae692318c73e30367cbc5c2a

namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.IO;
	using System.IO.Compression;
	using System.Text;

	/// <summary>
	/// TarGZ Extraction
	/// </summary>
	public static class TarGz
	{
		/// <summary>
		/// Extracts a <i>.tar.gz</i> archive to the specified directory.
		/// </summary>
		/// <param name="filename">The <i>.tar.gz</i> to decompress and extract.</param>
		/// <param name="outputDir">Output directory to write the files.</param>
		public static void ExtractTarGz(string filename, string outputDir)
		{
			using (FileStream stream = File.OpenRead(filename))
				ExtractTarGz(stream, outputDir);
		}

		/// <summary>
		/// Extracts a <i>.tar.gz</i> archive stream to the specified directory.
		/// </summary>
		/// <param name="stream">The <i>.tar.gz</i> to decompress and extract.</param>
		/// <param name="outputDir">Output directory to write the files.</param>
		public static void ExtractTarGz(Stream stream, string outputDir)
		{
			using (GZipStream gzip = new GZipStream(stream, CompressionMode.Decompress))
			{
				// removed convertation to MemoryStream
				ExtractTar(gzip, outputDir);
			}
		}

		/// <summary>
		/// Extractes a <c>tar</c> archive to the specified directory.
		/// </summary>
		/// <param name="filename">The <i>.tar</i> to extract.</param>
		/// <param name="outputDir">Output directory to write the files.</param>
		public static void ExtractTar(string filename, string outputDir)
		{
			using (FileStream stream = File.OpenRead(filename))
				ExtractTar(stream, outputDir);
		}

		/// <summary>
		/// Extractes a <c>tar</c> archive to the specified directory.
		/// </summary>
		/// <param name="stream">The <i>.tar</i> to extract.</param>
		/// <param name="outputDir">Output directory to write the files.</param>
		public static void ExtractTar(Stream stream, string outputDir)
		{
			var buffer = new byte[100];
			// store current position here
			long pos = 0;
			while (true)
			{
				pos += stream.Read(buffer, 0, 100);
				var name = Encoding.ASCII.GetString(buffer).Trim('\0');

				if (String.IsNullOrWhiteSpace(name))
					break;
				pos += FakeSeekForward(stream, 24);

				pos += stream.Read(buffer, 0, 12);
				var size = Convert.ToInt64(Encoding.UTF8.GetString(buffer, 0, 12).Trim('\0').Trim(), 8);
				pos += FakeSeekForward(stream, 376);

				var output = Path.Combine(outputDir, name);

				bool isFolder = name[name.Length - 1] == '/' || name[name.Length - 1] == '\\';
				if (!Directory.Exists(Path.GetDirectoryName(output)))
					Directory.CreateDirectory(Path.GetDirectoryName(output));

				if (!name.Equals("./", StringComparison.InvariantCulture) && !isFolder)
				{
					using (var str = File.Open(output, FileMode.OpenOrCreate, FileAccess.Write))
					{
						var buf = new byte[size];
						pos += stream.Read(buf, 0, buf.Length);
						str.Write(buf, 0, buf.Length);
					}
				}

				var offset = (int)(512 - (pos % 512));
				if (offset == 512)
					offset = 0;
				pos += FakeSeekForward(stream, offset);
			}
		}

		public static int SeekForward(this Stream stream, int offset)
		{
			return FakeSeekForward(stream, offset);
		}

		private static int FakeSeekForward(Stream stream, int offset)
		{
			if (stream.CanSeek)
				return (int)stream.Seek(offset, SeekOrigin.Current);
			else
			{
				int bytesRead = 0;
				var buffer = new byte[offset];
				while (bytesRead < offset)
				{
					int read = stream.Read(buffer, bytesRead, offset - bytesRead);
					if (read == 0)
						throw new EndOfStreamException();
					bytesRead += read;
				}
				return bytesRead;
			}
		}

		/// <summary>
		/// Get package.json from stream of package.tgz
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string GetPackageJsonFromTgz(Stream stream)
		{
			using (var gzipStream = new GZipStream(stream, CompressionMode.Decompress))
			{
				var buffer = new byte[100];

				long pos = 0;
				while (true)
				{
					pos += gzipStream.Read(buffer, 0, 100);
					string name = Encoding.ASCII.GetString(buffer).Trim('\0');

					if (String.IsNullOrWhiteSpace(name))
						break;

					pos += FakeSeekForward(gzipStream, 24);

					pos += gzipStream.Read(buffer, 0, 12);

					var size = Convert.ToInt64(Encoding.UTF8.GetString(buffer, 0, 12).Trim('\0').Trim(), 8);

					pos += FakeSeekForward(gzipStream, 376);


					//File
					if (name.ToLower() == "package/package.json")
					{
						var buf = new byte[size];
						pos += gzipStream.Read(buf, 0, buf.Length);

						return Encoding.UTF8.GetString(buf);
					}
					else
					{
						pos += FakeSeekForward(gzipStream, (int)size);
					}

					var offset = 512 - (pos % 512);
					if (offset == 512)
						offset = 0;

					pos += FakeSeekForward(gzipStream, (int)offset);
				}
			}

			return null;
		}

		/// <summary>
		/// Get package.json from package.tgz
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static string GetPackageJsonFronTgz(string fileName)
		{
			DirectoryInfo directory = Directory.GetParent(fileName);

			if (directory.Exists == false || File.Exists(fileName) == false)
			{
				return null;
			}

			using (var stream = File.OpenRead(fileName))
			{
				return GetPackageJsonFromTgz(stream);
			}
		}

		/// <summary>
		/// Get package.json from data of package.tgz
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string GetPackageJsonFromTgz(byte[] data)
		{
			using (var stream = new MemoryStream(data))
			{
				return GetPackageJsonFromTgz(stream);
			}
		}
	}
}
