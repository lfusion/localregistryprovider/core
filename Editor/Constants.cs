/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	/// <summary>
	/// Contain Local Registry Provider package inforamtions
	/// </summary>
	public static class Constants
	{
		public const string PackageName = "com.lfusion.localregistryprovider.core";
		public const string RegistryName = "LFusion Local Registry Provider";

		public const string PackageDisplayedName = "Local Registry Provider";
		public const string PackageShortDisplayedName = "Local Registry";

		public const string PackageManagerSettingsName = "Package Manager";
	}
}