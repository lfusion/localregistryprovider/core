/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using UnityEngine;

	using System.Linq;

	[Serializable]
	public class SearchResponsePackageInfo : IToJson
	{
		[Serializable]
		public struct DistTags
		{
			[SerializeField] private string latest;

			public string Latest { get => latest; set => latest = value; }
		}

		[SerializeField] private string name;
		[SerializeField] private string displayName;
		[SerializeField] private string description;
		//[SerializeField] private DistTags dist_tags;
		//[SerializeField] private AuthorInfo[] maintainers = new AuthorInfo[] { new AuthorInfo { Name = "LFusion", Email = "LFusion" } };
		[SerializeField] private AuthorInfo author;

		public string Name { get => name; set => name = value; }
		public string DisplayName { get => displayName; set => displayName = value; }
		public string Description { get => description; set => description = value; }
		//public DistTags Dist_tags { get => dist_tags; set => dist_tags = value; }
		public AuthorInfo Author { get => author; set => author = value; }

		public SearchResponsePackageInfo(PackageInfo packageInfo)
		{
			name = packageInfo.Name;

			var latest = packageInfo.Versions.Last();

			displayName = latest.Value.DisplayName;
			description = latest.Value.Description;
			//dist_tags.Latest = latest.Value.Version;
			author = latest.Value.Author;
		}

		public string ToJson(bool pretty = false)
		{
			return JsonUtility.ToJson(this, pretty).Replace("dist_tags", "dist-tags");
		}
	}
}


