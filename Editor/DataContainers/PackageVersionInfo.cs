/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using UnityEngine;

	[Serializable]
	public class PackageVersionInfo : IToJson, IParseJson
	{
		#region Fields
		[SerializeField] private string name;
		[SerializeField] private string displayName;
		[SerializeField] private string version;
		[SerializeField] private string description;
		[SerializeField] private Dictionary<string, string> dependencies = new Dictionary<string, string>();
		[SerializeField] private AuthorInfo author;
		[SerializeField] private DistInfo dist;
		#endregion Fieds

		#region Properties
		public string Name { get => name; set => name = value; }
		public string DisplayName { get => displayName; set => displayName = value; }
		public string Version { get => version; set => version = value; }
		public string Description { get => description; set => description = value; }
		public Dictionary<string, string> Dependencies { get => dependencies; /*set => dependencies = value; */}
		public AuthorInfo Author { get => author; set => author = value; }
		public DistInfo Dist { get => dist; set => dist = value; }
		#endregion Properties

		#region Methods
		internal void GenerateDistTarballUrl(string server)
		{
			dist.Tarball = server + name + "/" + version + "/" + string.Format("{0}-{1}.tgz", name, version);
			//dist.Tarball = System.IO.Path.Combine(server, name, version, string.Format("{0}-{1}.tgz", name, version));
		}

		public void ParseJson(string json)
		{
			JsonUtility.FromJsonOverwrite(json, this);

			int dependenciesIndex = json.IndexOf("\"dependencies\"");
			if (dependenciesIndex > -1)
			{
				string dependenciesDictionaryJson = Helpers.GetJsonPart(json, dependenciesIndex);
				dependencies.ParseJson(dependenciesDictionaryJson);
			}
		}

		public string ToJson(bool pretty = false)
		{
			StringBuilder jsonBuilder = new StringBuilder("{");

			Helpers.Append(jsonBuilder, "name", name, true);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "displayName", displayName.LineBreakToJson(), true);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "version", version.LineBreakToJson(), true);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "description", description.LineBreakToJson(), true);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "dependencies", dependencies.ToJson(), false);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "author", author.ToJson(), false);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "dist", dist.ToJson(), false);

			jsonBuilder.Append("}");

			if (pretty)
			{
				Helpers.PretifyJson(jsonBuilder);
			}

			return jsonBuilder.ToString();
		}
		#endregion Methods
	}
}
