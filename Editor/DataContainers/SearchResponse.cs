/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	[Serializable]
	public class SearchResponse : IToJson
	{
		[SerializeField] private List<SearchResponseItem> objects = new List<SearchResponseItem>();
		[SerializeField] private int total;
		[SerializeField] private string time;

		public List<SearchResponseItem> Objects { get => objects; set => objects = value; }
		public int Total { get => total; set => total = value; }
		public string Time { get => time; set => time = value; }

		public SearchResponse(List<SearchResponseItem> objects)
		{
			this.objects = objects;
			this.total = objects.Count;
			this.time = DateTime.Now.ToString();
		}

		public string ToJson(bool pretty = false)
		{
			return JsonUtility.ToJson(this, pretty).Replace("dist_tags", "dist-tags");
		}

	}
}
