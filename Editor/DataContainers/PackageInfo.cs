/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using UnityEngine;

	[Serializable]
	public class PackageInfo : IToJson, IParseJson
	{

		[SerializeField] private string name;
		[SerializeField] private Dictionary<string, PackageVersionInfo> versions = new Dictionary<string, PackageVersionInfo>();
		[SerializeField] private Dictionary<string, string> time = new Dictionary<string, string>();

		public string Name { get => name; set => name = value; }
		public Dictionary<string, PackageVersionInfo> Versions { get => versions; /*set => versions = value; */}

		public Dictionary<string, string> Time { get => time; }

		public void AddVersion(string version, AuthorInfo? author = null, DistInfo? dist = null)
		{
			AuthorInfo authorInfo = author == null ? new AuthorInfo() { Name = "LFusion Provider Default" } : (AuthorInfo)author;
			DistInfo distInfo = dist == null ? new DistInfo("", "") : (DistInfo)dist;

			if (versions.ContainsKey(version))
			{
				versions[version].Version = version;
				versions[version].Author = authorInfo;
				versions[version].Dist = distInfo;
			}
			else
			{
				versions.Add(version, new PackageVersionInfo() { Name = name, Version = version, Dist = distInfo, Author = authorInfo });
			}
		}

		public void RemoveVersion(string version)
		{
			if (versions.ContainsKey(version))
			{
				versions.Remove(version);
			}
		}

		public void ParseJson(string json)
		{
			JsonUtility.FromJsonOverwrite(json, this);

			int versionsIndex = json.IndexOf("\"versions\"");
			if (versionsIndex > -1)
			{
				var versionsDictionaryJson = Helpers.GetJsonPart(json, versionsIndex);
				versions.ParseJson(versionsDictionaryJson);
			}

			int timeIndex = json.IndexOf("\"time\"");
			if (timeIndex > -1)
			{
				var timeDictionaryJson = Helpers.GetJsonPart(json, timeIndex);
				time.ParseJson(timeDictionaryJson);
			}
		}

		public string ToJson(bool pretty = false)
		{
			StringBuilder jsonBuilder = new StringBuilder("{");

			Helpers.Append(jsonBuilder, "name", name.LineBreakToJson(), true);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "versions", versions.ToJson(), false);
			jsonBuilder.Append(",");

			Helpers.Append(jsonBuilder, "time", time.ToJson(), false);

			jsonBuilder.Append("}");

			if (pretty)
			{
				Helpers.PretifyJson(jsonBuilder);
			}

			return jsonBuilder.ToString();
		}
	}
}
