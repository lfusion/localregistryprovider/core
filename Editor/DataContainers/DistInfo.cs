/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using UnityEngine;

	[Serializable]
	public struct DistInfo : IToJson
	{
		#region Fields
		[SerializeField] private string tarball;
		[SerializeField] private string shasum;
		#endregion Fields

		#region Properties
		public string Tarball { get => tarball; set => tarball = value; }
		public string Shasum { get => shasum; set => shasum = value; }
		#endregion Properties

		#region Methods
		public DistInfo(string tarball, string shasum)
		{
			this.tarball = tarball;
			this.shasum = shasum;
		}

		public string ToJson(bool pretty = false)
		{
			return JsonUtility.ToJson(this, pretty);
		}
		#endregion Methods
	}
}
