/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using UnityEditor;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;

	[Serializable]
	public class AddDependencyAction : IAction
	{
		[SerializeField] private string m_dependencyName = "com.defaultcompany.foo";
		[SerializeField] private string m_dependencyVersion = "*";

		public void Execute(PackageVersionInfo packageVersionInfo)
		{
			if (packageVersionInfo.Dependencies.ContainsKey(m_dependencyName))
			{
				packageVersionInfo.Dependencies[m_dependencyName] = m_dependencyVersion;
			}
			else
			{
				packageVersionInfo.Dependencies.Add(m_dependencyName, m_dependencyVersion);
			}
		}
	}

	namespace UI
	{
		[CustomPropertyDrawer(typeof(AddDependencyAction))]
		public class AddDependencyActionDrawerUIE : UnityEditor.PropertyDrawer
		{
			public override VisualElement CreatePropertyGUI(SerializedProperty property)
			{
				// Create property container element.
				var container = new VisualElement();

				container.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);

				var label = new Label()
				{
					text = "Add Dependency"
				};

				StyleSheet disablePropertyLabelStyle = Helpers.LoadDisablePropertyLabelStyle();

				var packageNameField = new PropertyField(property.FindPropertyRelative("m_dependencyName"));
				packageNameField.styleSheets.Add(disablePropertyLabelStyle);
				packageNameField.style.flexGrow = 1;

				var versionField = new PropertyField(property.FindPropertyRelative("m_dependencyVersion"));
				versionField.styleSheets.Add(disablePropertyLabelStyle);
				versionField.style.width = new StyleLength(100);

				container.Add(label);
				container.Add(packageNameField);
				container.Add(versionField);

				return container;
			}
		}
	}

}