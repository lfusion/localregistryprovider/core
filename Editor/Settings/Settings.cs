/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;

	[FilePath("ProjectSettings/LFusion/LocalRegistryProviderSettings.asset", FilePathAttribute.Location.ProjectFolder)]
	class Settings : ScriptableSingleton<Settings>
	{
		#region Fields
		[SerializeField] private string m_lastVersion = null;
		[SerializeField] private EmbedState m_embedState = EmbedState.Disembedded;
		[SerializeField] private int m_serverPort = 7412;
		[SerializeField] private bool m_autoUpdateRegistry = false;
		[SerializeField] private int m_scopeAccuracy = 2;
		[SerializeField] private List<Rule> m_rules = new List<Rule>();
		[SerializeField] private bool m_debugMode = false;
		[SerializeField] private ClearCacheRef m_lastUpmCacheClear = new ClearCacheRef(0, new DateTime(0));
		[SerializeField] private ClearCacheRef m_lastLocalServerCacheClear = new ClearCacheRef(0, new DateTime(0));
		#endregion Fields

		#region Properties
		public string LastVersion { get => m_lastVersion; }
		public EmbedState EmbedState { get => m_embedState; }
		public List<Rule> Rules { get => m_rules; }
		public bool DebugMode { get => m_debugMode; }
		public int ServerPort { get => m_serverPort; protected set => m_serverPort = value; }
		public bool AutoUpdateRegistry { get => m_autoUpdateRegistry; protected set => m_autoUpdateRegistry = value; }
		public int ScopeAccuracy { get => m_scopeAccuracy; protected set => m_scopeAccuracy = value; }
		public ClearCacheRef LastUpmCacheClear { get => m_lastUpmCacheClear; internal set => m_lastUpmCacheClear = value; }
		public ClearCacheRef LastLocalServerCacheClear { get => m_lastLocalServerCacheClear; internal set => m_lastLocalServerCacheClear = value; }
		#endregion Properties

		#region Events
		private Action<EmbedState> m_embedStateChanged;
		public event Action<EmbedState> EmbedStateChanged
		{
			add
			{
				m_embedStateChanged -= value;
				m_embedStateChanged += value;
			}

			remove
			{
				m_embedStateChanged -= value;
			}
		}
		#endregion Events

		#region Methods
		private void OnEnable()
		{
			hideFlags &= ~HideFlags.NotEditable;
		}

		private void OnDisable()
		{
			Save();
		}

		/// <summary>
		/// Save the timeline project settings file in the project directory.
		/// </summary>
		public void Save()
		{
			Save(true);
		}

		public static SerializedObject GetSerialized()
		{
			return new SerializedObject(instance);
		}

		internal void SetLastVersion(string version)
		{
			m_lastVersion = version;
			Save();
		}

		internal void SetEmbedState(EmbedState embedState)
		{
			m_embedState = embedState;
			m_embedStateChanged?.Invoke(m_embedState);
			Save();
		}
		#endregion Methods
	}
}