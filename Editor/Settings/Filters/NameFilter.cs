/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using UnityEngine;
	using UnityEngine.UIElements;
	using UnityEditor.UIElements;
	using System;

	[Serializable]
	public class NameFilter : IFilter
	{
		public enum Mode
		{
			Contain,
			NotContain
		}

		[SerializeField] private string m_name = "com.defaultcompany.foo";
		[SerializeField] private Mode m_mode;

		public bool Validate(PackageVersionInfo packageVersionInfo)
		{
			if (m_mode == Mode.Contain)
			{
				return packageVersionInfo.Name.Contains(m_name);
			}
			else
			{
				return !packageVersionInfo.Name.Contains(m_name);
			}
		}
	}

	[UnityEditor.CustomPropertyDrawer(typeof(NameFilter))]
	public class NameFilterDrawerUIE6565 : UnityEditor.PropertyDrawer
	{
		public override VisualElement CreatePropertyGUI(UnityEditor.SerializedProperty property)
		{
			// Create property container element.
			var container = new VisualElement();

			container.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);

			var label = new Label()
			{
				text = "Package Name"
			};

			StyleSheet disablePropertyLabelStyle = UI.Helpers.LoadDisablePropertyLabelStyle();

			var modeField = new PropertyField(property.FindPropertyRelative("m_mode"));
			modeField.styleSheets.Add(disablePropertyLabelStyle);

			var nameField = new PropertyField(property.FindPropertyRelative("m_name"));
			nameField.styleSheets.Add(disablePropertyLabelStyle);
			nameField.style.flexGrow = 1;

			container.Add(label);
			container.Add(modeField);
			container.Add(nameField);

			return container;
		}
	}
}