/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public static class Filters
	{
		private static List<Type> s_filters = new List<Type>();

		public static IReadOnlyList<Type> List { get => s_filters.AsReadOnly(); }

		public static bool Validate(PackageVersionInfo packageVersionInfo, IList<IFilter> filters)
		{
			for (int i = 0; i < filters.Count; i++)
			{
				if (filters[i].Validate(packageVersionInfo) == false)
				{
					return false;
				}
			}

			return true;
		}

		static Filters()
		{
			var iFilterType = typeof(IFilter);
			var types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => iFilterType.IsAssignableFrom(p) && !p.IsInterface);

			foreach (var type in types)
			{
				s_filters.Add(type);
			}
		}
	}
}