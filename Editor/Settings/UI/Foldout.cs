﻿/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using UnityEngine;
	using UnityEngine.UIElements;

	public class Foldout : VisualElement
	{
		#region UIElement
		public new class UxmlFactory : UxmlFactory<Foldout, UxmlTraits> { }

		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			readonly UxmlStringAttributeDescription k_label = new UxmlStringAttributeDescription { name = "label" };
			readonly UxmlBoolAttributeDescription k_value = new UxmlBoolAttributeDescription { name = "value", defaultValue = true };

			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);

				if (ve is Foldout f)
				{
					//f.text = m_Text.GetValueFromBag(bag, cc);
					//f.SetValueWithoutNotify(m_Value.GetValueFromBag(bag, cc));
					f.Value = k_value.GetValueFromBag(bag, cc);
					f.Label = k_label.GetValueFromBag(bag, cc);
				}
			}
		}
		#endregion UIElement

		#region Constantes
		private const string STYLE_FOLDOUT = "lfusion-foldout";
		private const string STYLE_FOLDOUT_HEADER = "lfusion-foldout-header";
		private const string STYLE_FOLDOUT_HEADER_CONTENT = "lfusion-header-content";
		private const string STYLE_ARROW = "lfusion-arrow";
		private const string STYLE_FOLDOUT_CONTENT = "lfusion-foldout-content";

		private const string CHAR_CLOSED = "►";
		private const string CHAR_OPENED = "▼";
		#endregion Constantes

		#region Fields
		[SerializeField] private bool m_value;

		private readonly Label k_headerArrow = new Label() { name = "arrow" };
		private readonly Label k_headerLabel = new Label() { name = "label" };
		private readonly VisualElement k_headerContainer = new VisualElement() { name = "header-content" };

		private readonly VisualElement k_content = new VisualElement() { name = "content" };
		#endregion Fields

		#region Properties
		public bool Value
		{
			get => m_value; set
			{
				if (m_value != value)
				{
					m_value = value;
					Refresh();
				}
			}
		}

		public string Label { get => k_headerLabel.text; set => k_headerLabel.text = value; }

		public override VisualElement contentContainer { get => k_content; }

		public VisualElement headerContainer
		{
			get => k_headerContainer;
		}
		#endregion Properties

		#region Methods
		#region Public
		public Foldout()
		{
			StyleSheet styleSheet = Helpers.LoadUiStyleSheet("LFusionFoldout.uss");
			styleSheets.Add(styleSheet);

			VisualElement header = new VisualElement() { name = "header" };

			hierarchy.Add(header);
			header.Add(k_headerArrow);
			header.Add(k_headerLabel);
			header.Add(k_headerContainer);

			header.RegisterCallback<ClickEvent>(OnClicked);

			hierarchy.Add(k_content);

			AddToClassList(STYLE_FOLDOUT);
			header.AddToClassList(STYLE_FOLDOUT_HEADER);
			k_headerArrow.AddToClassList(STYLE_ARROW);
			k_headerContainer.AddToClassList(STYLE_FOLDOUT_HEADER_CONTENT);
			k_content.AddToClassList(STYLE_FOLDOUT_CONTENT);

			Refresh();
		}

		public void RegisterChildContainer()
		{

		}

		public void Toggle()
		{
			Value = !Value;
		}
		#endregion Public
		#region Private
		private void Refresh()
		{
			k_headerArrow.text = m_value ? CHAR_OPENED : CHAR_CLOSED;
			k_content.style.display = m_value ? new StyleEnum<DisplayStyle>(DisplayStyle.Flex) : new StyleEnum<DisplayStyle>(DisplayStyle.None);
		}

		private void OnClicked(ClickEvent clickEvent)
		{
			Toggle();
		}
		#endregion Private
		#endregion Methods
	}
}