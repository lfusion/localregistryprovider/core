﻿/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using UnityEngine.UIElements;

	public class ProviderInfo : VisualElement
	{
		private IProvider m_provider = null;

		private Foldout m_container = new Foldout() { name = "ProviderContainer" };
		private Label m_descrition = new Label() { name = "ProviderDescription" };
		private Foldout m_packagesContainer = new Foldout() { name = "PackagesInfoContainer", Label = "Packages" };
		private Button m_settingsButton = new Button() { text = "☰" };

		public IProvider Provider
		{
			get => m_provider;
			set
			{
				if (m_provider == value)
				{
					return;
				}

				m_provider = value;
				Refresh();
			}
		}

		public ProviderInfo()
		{
			Add(m_container);
			m_container.Add(m_descrition);
			m_container.Add(m_packagesContainer);

			m_settingsButton.RegisterCallback<ClickEvent>(OnSettingsButtonClicked);
			m_container.headerContainer.Add(m_settingsButton);
		}

		private void Refresh()
		{
			if (m_provider == null)
			{
				return;
			}

			m_container.Label = m_provider.Name;
			m_descrition.text = m_provider.Description;

			m_settingsButton.tooltip = string.Format("Go to {0} settings", m_provider.Name);

			m_packagesContainer.Clear();

			PackageInfo[] packageInfos = m_provider.Packages;

			m_packagesContainer.headerContainer.Clear();

			int versionCount = 0;

			foreach (PackageInfo package in packageInfos)
			{
				PackageInfoElement packageInfoElement = new PackageInfoElement();
				packageInfoElement.PackageInfo = package;

				versionCount += package.Versions.Count;

				m_packagesContainer.Add(packageInfoElement);
			}
			m_packagesContainer.headerContainer.Add(new Label() { text = packageInfos.Length + " Packages | " + versionCount + " Versions" });
		}

		private void OnSettingsButtonClicked(ClickEvent e)
		{
			e.StopPropagation();
			m_provider.OpenSettings();
		}
	}
}