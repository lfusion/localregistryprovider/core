/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using System.Threading.Tasks;
	using UnityEngine;
	using UnityEngine.UIElements;

	public class BusyOverlay : VisualElement
	{
		#region Constants
		private const float LOGO_SCALE = 1.5f;
		#endregion Constants

		#region Fields
		private VisualElement m_logo = null;
		private Task m_animationTask = null;
		private bool m_isAnimate = false;
		#endregion Fields

		#region Methods
		public BusyOverlay()
		{
			VisualTreeAsset logoVisualTree = Helpers.LoadUiVisualTree("LFusionLogoHorizontal.uxml");

			style.alignContent = Align.Center;
			style.justifyContent = Justify.Center;
			style.position = Position.Absolute;
			style.left = 0;
			style.right = 0;
			style.bottom = 0;
			style.top = 0;

			style.backgroundColor = new Color(0.25f, 0.25f, 0.25f, 0.75f);

#if UNITY_2021_2_OR_NEWER
			logoVisualTree.CloneTree(this);
			m_logo = this.Q<VisualElement>("logo");
			m_logo.style.alignSelf = Align.Center;
			m_logo.style.scale = new Scale(Vector2.one * LOGO_SCALE);
#else // UNITY_2021_2_OR_NEWER == false
			m_logo = new VisualElement() { name = "logoPivot" };
			m_logo.style.alignSelf = Align.Center;
			m_logo.transform.scale = Vector3.one * LOGO_SCALE;
			Add(m_logo);

			logoVisualTree.CloneTree(m_logo);
			VisualElement logo = m_logo.Q<VisualElement>("logo");
			logo.style.position = Position.Absolute;
			logo.style.top = -15;
			logo.style.bottom = -15;
			logo.style.left = -15;
			logo.style.right = -15;

			m_logo.Add(logo);
#endif
			Hide();
		}
		~BusyOverlay()
		{
			Hide();
		}

		public void Display()
		{
			style.display = DisplayStyle.Flex;
			m_isAnimate = true;
			if (m_animationTask == null)
			{
				m_animationTask = Animate();
			}
		}

		public void Hide()
		{
			style.display = DisplayStyle.None;
			m_isAnimate = false;
		}

		private async Task Animate()
		{
			while (m_isAnimate)
			{
				double time = UnityEditor.EditorApplication.timeSinceStartup;
				float rotation = Mathf.Cos((float)(time % 1) * Mathf.PI);
#if UNITY_2021_2_OR_NEWER
				m_logo.style.rotate = new Rotate(rotation * 180);
#else // UNITY_2021_2_OR_NEWER == false
				m_logo.transform.rotation = Quaternion.Euler(0,0,rotation * 180);
#endif
				await Task.Yield();
			}
			m_animationTask = null;
		}
		#endregion Methods
	}
}