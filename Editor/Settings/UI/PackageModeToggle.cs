/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using System;
	using UnityEngine;
	using UnityEngine.UIElements;

	public class PackageModeToggle : BindableElement, INotifyValueChanged<int>
	{
		#region UIElement
		public new class UxmlFactory : UxmlFactory<PackageModeToggle, UxmlTraits> { }

		public new class UxmlTraits : BindableElement.UxmlTraits
		{
			readonly UxmlEnumAttributeDescription<EmbedState> k_value = new UxmlEnumAttributeDescription<EmbedState> { name = "value", defaultValue = EmbedState.Disembedded };

			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);

				if (ve is PackageModeToggle f)
				{
					//f.SetValueWithoutNotify(k_value.GetValueFromBag(bag, cc));
					f.value = (int)k_value.GetValueFromBag(bag, cc);
				}
			}
		}
		#endregion UIElement

		#region Constantes
		private const string STYLE_MAIN = "lfusion-package-mode-toggle";
		private const string STYLE_LABEL = "lfusion-package-mode-toggle-label";
		private const string STYLE_TEXT = "lfusion-package-mode-toggle-text";
		private const string STYLE_BUTTON = "lfusion-package-mode-toggle-button";
		private const string STYLE_IMAGE = "lfusion-package-mode-toggle-image";
		private const string STYLE_IMAGE_HOVER = "lfusion-package-mode-toggle-image-hover";

		private const string STYLE_BUSY = "busy";
		private const string STYLE_EMBEDDED = "embedded";

		private const string TOOLTIP_EMBEDDING = "Embed all packages installed from Local Registry Provider";
		private const string TOOLTIP_DISEMBEDDING = "Disembed all packages installed from Local Registry Provider";
		#endregion Constantes

		#region Events
		private Action m_embeddingRequested = null;
		public event Action EmbeddingRequested
		{
			add
			{
				m_embeddingRequested -= value;
				m_embeddingRequested += value;
			}
			remove
			{
				m_embeddingRequested -= value;
			}
		}

		private Action m_disembeddingRequested = null;
		public event Action DisembeddingRequested
		{
			add
			{
				m_disembeddingRequested -= value;
				m_disembeddingRequested += value;
			}
			remove
			{
				m_disembeddingRequested -= value;
			}
		}
		#endregion Events

		#region Fields
		private EmbedState m_embedState = EmbedState.Disembedded;

		private readonly Texture2D k_embeddingImage = null;
		private readonly Texture2D k_embeddedImage = null;
		private readonly Texture2D k_disembeddingImage = null;
		private readonly Texture2D k_disembeddedImage = null;

		private readonly TextElement k_label = new TextElement() { name = "label", text = "Package Mode" };
		private readonly TextElement k_stateText = new TextElement() { name = "state" };
		private readonly Button k_button = new Button() { name = "button" };
		private readonly Image k_stateImage = new Image() { name = "image-state", scaleMode = ScaleMode.ScaleToFit };
		private readonly Image k_stateImageHover = new Image() { name = "image-state-hover", scaleMode = ScaleMode.ScaleToFit };
		#endregion Fields

		#region Properties
		public int value { get => (int)m_embedState; set => ChangeEmbedStateValue((EmbedState)value); }
		#endregion Properties

		#region Methods
		#region Public
		public PackageModeToggle()
		{
			k_embeddingImage = Helpers.LoadUiImage("embedding_32x32.png");
			k_embeddedImage = Helpers.LoadUiImage("embedded_32x32.png");
			k_disembeddingImage = Helpers.LoadUiImage("disembedding_32x32.png");
			k_disembeddedImage = Helpers.LoadUiImage("disembedded_32x32.png");

			hierarchy.Add(k_label);
			hierarchy.Add(k_button);
			hierarchy.Add(k_stateText);

			k_button.Add(k_stateImage);
			k_button.Add(k_stateImageHover);

			k_button.clicked += OnButtonClicked;

			StyleSheet styleSheet = Helpers.LoadUiStyleSheet("LFusionPackageModeToggle.uss");
			styleSheets.Add(styleSheet);

			AddToClassList(STYLE_MAIN);
			k_label.AddToClassList(STYLE_LABEL);
			k_stateText.AddToClassList(STYLE_TEXT);
			k_button.AddToClassList(STYLE_BUTTON);
			k_stateImage.AddToClassList(STYLE_IMAGE);
			k_stateImageHover.AddToClassList(STYLE_IMAGE_HOVER);

			UpdateUI();
		}

		public void SetValueWithoutNotify(int newValue)
		{
			ChangeEmbedStateValue((EmbedState)newValue);
		}
		#endregion Public

		#region Private
		private void ChangeEmbedStateValue(EmbedState value)
		{
			if (m_embedState == value)
			{
				return;
			}
			m_embedState = value;

			UpdateUI();
		}

		private void UpdateUI()
		{
			k_stateText.text = m_embedState.ToString();

			switch (m_embedState)
			{
				case EmbedState.Disembedded:
					RemoveFromClassList(STYLE_BUSY);
					RemoveFromClassList(STYLE_EMBEDDED);
					k_stateImage.image = k_disembeddedImage;
					k_stateImageHover.image = k_embeddingImage;
					k_button.tooltip = TOOLTIP_EMBEDDING;
					break;
				case EmbedState.Embedding:
					AddToClassList(STYLE_BUSY);
					RemoveFromClassList(STYLE_EMBEDDED);
					k_stateImage.image = k_embeddingImage;
					k_stateImageHover.image = k_embeddingImage;
					k_button.tooltip = null;
					break;
				case EmbedState.Embedded:
					RemoveFromClassList(STYLE_BUSY);
					AddToClassList(STYLE_EMBEDDED);
					k_stateImage.image = k_embeddedImage;
					k_stateImageHover.image = k_disembeddingImage;
					k_button.tooltip = TOOLTIP_DISEMBEDDING;
					break;
				case EmbedState.Disembedding:
					AddToClassList(STYLE_BUSY);
					RemoveFromClassList(STYLE_EMBEDDED);
					k_stateImage.image = k_disembeddingImage;
					k_stateImageHover.image = k_disembeddingImage;
					k_button.tooltip = null;
					break;
				default:
					break;
			}
		}

		private void OnButtonClicked()
		{
			switch (m_embedState)
			{
				case EmbedState.Disembedded:
					m_embeddingRequested?.Invoke();
					break;
				case EmbedState.Embedding:
					break;
				case EmbedState.Embedded:
					m_disembeddingRequested?.Invoke();
					break;
				case EmbedState.Disembedding:
					break;
				default:
					break;
			}
		}
		#endregion Private
		#endregion Methods
	}
}