/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using UnityEngine.UIElements;

	public class PackageInfoElement : VisualElement
	{
		private PackageInfo m_packageInfo = null;

		private Foldout m_container = new Foldout() { Value = false };

		public PackageInfo PackageInfo
		{
			get => m_packageInfo;
			set
			{
				if (m_packageInfo == value)
				{
					return;
				}

				m_packageInfo = value;
				Refresh();
			}
		}

		public PackageInfoElement()
		{
			Add(m_container);
		}

		private void Refresh()
		{
			m_container.Clear();
			m_container.Label = m_packageInfo.Name;

			m_container.headerContainer.Clear();
			m_container.headerContainer.Add(new Label() { text = m_packageInfo.Versions.Count + " Versions" });

			foreach (var keyAndVersionInfo in m_packageInfo.Versions)
			{
				Label version = new Label();
				version.text = keyAndVersionInfo.Key;

				m_container.Add(version);
			}
		}
	}
}
