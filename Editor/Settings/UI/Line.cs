/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using UnityEngine;
	using UnityEngine.UIElements;

	public class Line : VisualElement
	{
		public new class UxmlFactory : UxmlFactory<Line, UxmlTraits> { }

		public new class UxmlTraits : VisualElement.UxmlTraits { }

		public Line()
		{
			VisualElement line = new VisualElement();
			VisualElement dot = new VisualElement();

			style.flexDirection = FlexDirection.Row;
			style.marginTop = 2;
			style.marginBottom = 2;

			Color color = new Color();
			ColorUtility.TryParseHtmlString("#0082B4", out color);

			line.style.borderBottomColor = color;
			line.style.borderBottomWidth = 1;
			line.style.flexGrow = 1;

			dot.style.height = 10;
			dot.style.width = 15;
			dot.style.backgroundColor = color;
			dot.style.borderBottomRightRadius = 5;
			dot.style.borderTopLeftRadius = 5;

			Add(line);
			Add(dot);
		}
	}
}