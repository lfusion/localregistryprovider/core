/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using System.Threading.Tasks;
	using UnityEditor;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;
	using Upm = UnityEditor.PackageManager;

	[InitializeOnLoad]
	public class UpmIntegration : Upm.UI.IPackageManagerExtension
	{
		#region Fields
		private static VisualElement s_visualElement = null;
		private static Foldout s_debugFoldout = null;
		private static Label s_debugJsonUpm = null;
		private static Label s_debugJsonProvider = null;
		private static Label s_providerInfo = null;

		private static ToolbarButton s_toolbarButtonSettings = null;
		private static PackageModeIndicator s_toolbarEmbedStatus = null;

		private static PackageModeIndicator s_titleEmbeddedTag = null;
		
		private static bool s_advancedIntegrationIsInit = false;
		#endregion Fields

		#region Methods
		static UpmIntegration()
		{
			Upm.UI.PackageManagerExtensions.RegisterExtension(new UpmIntegration());
		}

		public static VisualElement GetRoot(VisualElement element)
		{
			while (element != null && element.parent != null)
			{
				element = element.parent;
			}

			return element;
		}

		public VisualElement CreateExtensionUI()
		{
			s_advancedIntegrationIsInit = false;
			CreateUI();
			return s_visualElement;
		}

		public void OnPackageAddedOrUpdated(Upm.PackageInfo packageInfo)
		{

		}

		public void OnPackageRemoved(Upm.PackageInfo packageInfo)
		{

		}

		public async void OnPackageSelectionChange(Upm.PackageInfo packageInfo)
		{
			await TryInitAdvancedIntegration();

			s_debugFoldout.style.display = Settings.instance.DebugMode ? DisplayStyle.Flex : DisplayStyle.None;

			if (packageInfo == null)
			{
				s_visualElement.style.display = DisplayStyle.None;
				s_titleEmbeddedTag.style.display = DisplayStyle.None;
				return;
			}

			s_debugJsonUpm.text = JsonUtility.ToJson(packageInfo, true);
			PackageVersionInfo providerInfo = ProviderManager.GetPackageVersionInfo(packageInfo.name, packageInfo.version);
			if (providerInfo != null)
			{
				s_debugJsonProvider.text = providerInfo.ToJson(true);
				s_visualElement.style.display = DisplayStyle.Flex;

				s_providerInfo.text = string.Format("This package come from: {0}", ProviderManager.GetPackageProvider(providerInfo).Name);
				s_titleEmbeddedTag.style.display = DisplayStyle.Flex;

			}
			else
			{
				s_debugJsonProvider.text = "";
				s_visualElement.style.display = DisplayStyle.None;
				s_titleEmbeddedTag.style.display = DisplayStyle.None;
			}
		}

		private void CreateUI()
		{
			if (s_visualElement == null)
			{
				s_visualElement = new VisualElement()
				{
					name = "LFusion_LocalRegistryProviderExtension"
				};
			}

			if (s_debugFoldout == null)
			{
				s_debugFoldout = new Foldout()
				{
					name = "debugFoldout",
					Label = "Local Registry Provider Debug - PackageInfo",
					Value = false
				};
			}

			if (s_debugJsonUpm == null)
			{
				s_debugJsonUpm = new Label()
				{
					name = "upmPackageDebugInfo"
				};
			}
			

			if (s_debugJsonProvider == null)
			{
				s_debugJsonProvider = new Label()
				{
					name = "providerPackageDebugInfo"
				};
			}

			Foldout foldout = new Foldout
			{
				Label = "Package Manager Cache",
				Value = false
			};
			foldout.Add(s_debugJsonUpm);

			Foldout foldout2 = new Foldout
			{
				Label = "Local Provider Data",
				Value = false
			};
			foldout2.Add(s_debugJsonProvider);

			s_debugFoldout.Add(foldout);
			s_debugFoldout.Add(foldout2);

			if (s_providerInfo == null)
			{
				s_providerInfo = new Label()
				{
					name = "providerInfo"
				};
			}

			s_visualElement.Add(new Line());
			s_visualElement.Add(s_providerInfo);
			s_visualElement.Add(s_debugFoldout);
		}

		private async Task TryInitAdvancedIntegration()
		{
			if (s_advancedIntegrationIsInit)
			{
				return;
			}

			SerializedObject settings = Settings.GetSerialized();

			TemplateContainer root = null;
			Toolbar toolbar = null;

			// Wait UPM UI fully initialized
			while (root == null || toolbar == null)
			{
				root = GetRoot(s_visualElement).Q<TemplateContainer>();
				toolbar = root.Q<Toolbar>();
				if (root == null || toolbar == null)
				{
					await Task.Yield();
				}
			}

			if (s_toolbarButtonSettings == null)
			{
				s_toolbarButtonSettings = new ToolbarButton()
				{
					name = "LRP_shortcut"
				};
			}

			if (s_toolbarEmbedStatus == null)
			{
				s_toolbarEmbedStatus = new PackageModeIndicator()
				{
					name = "packageModeIndicator"
				};
			}

			if (s_titleEmbeddedTag == null)
			{
				s_titleEmbeddedTag = new PackageModeIndicator()
				{
					name = "embeddedTag",
					UseColor = true
				};
			}

			ToolbarSpacer toolbarSpacer = toolbar.Q<ToolbarSpacer>();

			toolbar.Insert(toolbar.IndexOf(toolbarSpacer) + 1, s_toolbarButtonSettings);
			s_toolbarButtonSettings.Add(s_toolbarEmbedStatus);
			s_toolbarButtonSettings.style.paddingLeft = s_toolbarButtonSettings.style.paddingRight = 0;

			s_toolbarButtonSettings.clicked -= OnPackageModeIndicatorClicked;
			s_toolbarButtonSettings.clicked += OnPackageModeIndicatorClicked;

			s_toolbarEmbedStatus.BindProperty(settings.FindProperty("m_embedState"));
			s_toolbarEmbedStatus.Bind(settings);

			VisualElement titleCtn = root.Q<VisualElement>("detailTitle").parent;
#if UNITY_2022_2_OR_NEWER
			titleCtn.Insert(1, s_titleEmbeddedTag);
#else // UNITY_2022_2_OR_NEWER == false
			titleCtn.Add(s_titleEmbeddedTag);
#endif

			s_titleEmbeddedTag.BindProperty(settings.FindProperty("m_embedState"));
			s_titleEmbeddedTag.Bind(settings);
			s_titleEmbeddedTag.style.height = 28;
			s_titleEmbeddedTag.style.width = 28;

			s_advancedIntegrationIsInit = true;
		}

		private void OnPackageModeIndicatorClicked()
		{
			SettingsService.OpenProjectSettings(Helpers.SettingsPath());
		}
#endregion Methods
	}
}