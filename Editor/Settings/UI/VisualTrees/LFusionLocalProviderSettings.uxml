<!--

    LFusion Local Registry Provider is used to create a registry provider linked to the project.
    Packages can be added by extensions implementing a provider.
    Copyright (C) 2024  Lucas Barbé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
-->
<ui:UXML xmlns:ui="UnityEngine.UIElements" xmlns:uie="UnityEditor.UIElements" xsi="http://www.w3.org/2001/XMLSchema-instance" engine="UnityEngine.UIElements" editor="UnityEditor.UIElements" noNamespaceSchemaLocation="../../../../UIElementsSchema/UIElements.xsd" editor-extension-mode="True">
    <Style src="project://database/Assets/LFusion/Core/package/Editor/Settings/UI/Styles/LFusionSettingsContainer.uss?fileID=7433441132597879392&amp;guid=91fe8dca75826a84bb9a8166a4f5805d&amp;type=3#LFusionSettingsContainer" />
    <LFusion.LocalRegistryProvider.UI.PackageModeToggle binding-path="m_embedState" name="packageModeToggle" class="mb-4" />
    <ui:Foldout text="General" name="generalContainer" class="mb-4">
        <uie:IntegerField label="Provider Port" value="7412" name="serverPort" binding-path="m_serverPort" />
        <ui:Toggle label="Auto Update Registry" name="autoUpdateField" binding-path="m_autoUpdateRegistry">
            <ui:Button text="↻" display-tooltip-when-elided="true" name="updateRegistryBtn" tooltip="Update Registry" />
        </ui:Toggle>
        <uie:IntegerField label="Scope Accuracy" value="2" name="scopeAccuracyField" binding-path="m_scopeAccuracy" />
        <ui:Button text="Apply now" display-tooltip-when-elided="true" name="applyPortBtn" class="align-self-end mt-4" />
    </ui:Foldout>
    <ui:Foldout text="Providers" name="providersContainer" class="mb-4" />
    <ui:Foldout text="Rules" name="rulesFoldout" value="true" class="mb-4">
        <ui:VisualElement name="rulesSettingsContainer" style="flex-direction: row; flex-grow: 1; max-height: none; min-height: 360px;">
            <ui:VisualElement name="rulesContainer" style="width: 240px; min-height: auto; height: auto; border-left-width: 1px; border-right-width: 1px; border-top-width: 1px; border-bottom-width: 1px; border-left-color: rgb(0, 0, 0); border-right-color: rgb(0, 0, 0); border-top-color: rgb(0, 0, 0); border-bottom-color: rgb(0, 0, 0);">
                <uie:Toolbar>
                    <ui:Label text="Rules" display-tooltip-when-elided="true" style="-unity-font-style: bold; -unity-text-align: middle-left;" />
                    <uie:ToolbarSpacer style="flex-grow: 1;" />
                    <uie:ToolbarButton text="+" display-tooltip-when-elided="true" name="addBtn" class="border-right-0" style="-unity-font-style: bold;" />
                    <uie:ToolbarButton text="-" display-tooltip-when-elided="true" name="removeBtn" class="border-right-0" style="-unity-font-style: bold;" />
                </uie:Toolbar>
                <ui:ListView focusable="true" name="rulesList" reorderable="true" show-alternating-row-backgrounds="ContentOnly" show-bound-collection-size="false" style="flex-grow: 1;" />
            </ui:VisualElement>
            <ui:VisualElement name="ruleSettingsContainer" style="flex-shrink: 1; flex-grow: 1; border-left-width: 1px; border-right-width: 1px; border-top-width: 1px; border-bottom-width: 1px; border-left-color: rgb(0, 0, 0); border-right-color: rgb(0, 0, 0); border-top-color: rgb(0, 0, 0); border-bottom-color: rgb(0, 0, 0);">
                <uie:Toolbar>
                    <ui:TextField picking-mode="Ignore" label="Rule Name" value="New Rule" text="New Rule" name="ruleName" style="flex-grow: 1;" />
                </uie:Toolbar>
                <ui:VisualElement name="filtersContainer" style="flex-grow: 1;">
                    <uie:Toolbar>
                        <ui:Label text="Filters" display-tooltip-when-elided="true" style="-unity-font-style: bold; -unity-text-align: middle-left;" />
                        <uie:ToolbarSpacer name="ToolbarSpacer" style="flex-grow: 1;" />
                        <uie:ToolbarMenu display-tooltip-when-elided="true" text="+ " name="addMenu" class="border-right-0" style="-unity-font-style: bold;" />
                        <uie:ToolbarButton text="-" display-tooltip-when-elided="true" name="removeBtn" class="border-right-0" style="-unity-font-style: bold;" />
                    </uie:Toolbar>
                    <ui:ListView focusable="true" reorderable="true" name="filtersList" show-bound-collection-size="false" show-alternating-row-backgrounds="ContentOnly" header-title="Filters" style="flex-grow: 1; min-height: auto; height: auto;" />
                </ui:VisualElement>
                <ui:VisualElement name="actionsContainer" style="flex-grow: 1;">
                    <uie:Toolbar class="border-top-1">
                        <ui:Label text="Actions" display-tooltip-when-elided="true" style="-unity-font-style: bold; -unity-text-align: middle-left;" />
                        <uie:ToolbarSpacer style="flex-grow: 1;" />
                        <uie:ToolbarMenu display-tooltip-when-elided="true" name="addMenu" text="+ " class="border-right-0" style="-unity-font-style: bold;" />
                        <uie:ToolbarButton text="-" display-tooltip-when-elided="true" name="removeBtn" class="border-right-0" style="-unity-font-style: bold;" />
                    </uie:Toolbar>
                    <ui:ListView focusable="true" name="actionsList" show-bound-collection-size="false" reorderable="true" show-alternating-row-backgrounds="ContentOnly" style="flex-grow: 1;" />
                </ui:VisualElement>
            </ui:VisualElement>
        </ui:VisualElement>
    </ui:Foldout>
    <ui:Foldout text="Advanced Settings" name="advancedFoldout" value="false">
        <ui:VisualElement style="flex-direction: row; align-items: center; flex-grow: 1; flex-shrink: 0; margin-left: 3px;">
            <ui:Label text="Standalone Server" display-tooltip-when-elided="true" />
            <ui:Button text="Create" display-tooltip-when-elided="true" name="createStandaloneServerBtn" class="button-stick-right" />
            <ui:Button text="Clean" display-tooltip-when-elided="true" name="cleanStandaloneServerBtn" class="button-stick-left button-stick-right" />
            <ui:Button text="Delete" display-tooltip-when-elided="true" name="deleteStandaloneServerBtn" class="button-stick-left" />
        </ui:VisualElement>
        <ui:VisualElement name="clearCacheContainer" style="flex-direction: row; height: 20px; align-items: center; justify-content: flex-start; flex-shrink: 0; margin-left: 3px;">
            <ui:Label text="Clear cache " display-tooltip-when-elided="true" />
            <ui:Button text="Unity" display-tooltip-when-elided="true" name="clearUnityCacheBtn" class="button-stick-right" />
            <ui:Button text="Local Server" display-tooltip-when-elided="true" name="clearLocalServerCacheBtn" class="button-stick-left button-stick-right" />
            <ui:Button text="Both" display-tooltip-when-elided="true" name="clearBothCacheBtn" class="button-stick-left" />
            <ui:VisualElement name="spacer" style="flex-grow: 1;" />
            <ui:Toggle label="Save force sync" name="saveForceSyncToggle" />
        </ui:VisualElement>
        <uie:PropertyField binding-path="m_debugMode" />
    </ui:Foldout>
</ui:UXML>
