/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using System;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEditor.UIElements;
	using UnityEngine;
	using UnityEngine.UIElements;
	using Core = LocalRegistryProvider;
	using UUI = UnityEngine.UIElements;

	// Register a SettingsProvider using UIElements for the drawing framework:
	static class LFusionLocalProviderSettingsUIElementsRegister
	{
		private static readonly HashSet<string> sk_keywords = new HashSet<string>(new string[] 
		{ 
			"LFusion",
			"Local",
			"Registry",
			"Provider",
			"Packages"
		});

		private static PackageModeToggle s_packageModeToggle = null;
		private static Button s_applyBtn = null;
		private static Button s_updateRegistryBtn = null;

		private static Button s_createStandaloneServerBtn = null;
		private static Button s_cleanStandaloneServerBtn = null;
		private static Button s_deleteStandaloneServerBtn = null;

		private static Button s_clearUnityCacheBtn = null;
		private static Button s_refreshServerCacheBtn = null;
		private static Button s_refreshBothCacheBtn = null;

		private static Toggle s_saveForceSyncToggle = null;

		[SettingsProvider]
		public static SettingsProvider CreateMyCustomSettingsProvider()
		{
			// First parameter is the path in the Settings window.
			// Second parameter is the scope of this setting: it only appears in the Settings window for the Project scope.
			var provider = new SettingsProvider(Helpers.SettingsPath(), SettingsScope.Project)
			{
				label = Constants.PackageShortDisplayedName,
				// activateHandler is called when the user clicks on the Settings item in the Settings window.
				activateHandler = (searchContext, rootElement) =>
				{
					Helpers.LoadSettingsContainer(rootElement, Constants.PackageDisplayedName, Core.Helpers.GetInstalledPackageVersion(Constants.PackageName));
					Helpers.LoadSettingsTemplate(rootElement, Helpers.LoadUiVisualTree("LFusionLocalProviderSettings.uxml"));

					SerializedObject settings = Settings.GetSerialized();

					s_packageModeToggle = rootElement.Q<PackageModeToggle>("packageModeToggle");
					s_packageModeToggle.EmbeddingRequested += OnEmbeddingRequested;
					s_packageModeToggle.DisembeddingRequested += OnDisembeddingRequested;

					UUI.Foldout generalContainer = rootElement.Q<UUI.Foldout>("generalContainer");

					s_applyBtn = generalContainer.Q<Button>("applyPortBtn");
					s_applyBtn.SetEnabled(Registry.UpdateScopedRegistryIsRunning == false);
					s_applyBtn.clicked += OnApplyBtnClicked;

					s_updateRegistryBtn = generalContainer.Q<Button>("updateRegistryBtn");
					s_updateRegistryBtn.SetEnabled(Registry.UpdateScopedRegistryIsRunning == false);
					s_updateRegistryBtn.clicked += OnUpdateRegistryBtnClicked;

					UUI.Foldout providersContainer = rootElement.Q<UUI.Foldout>("providersContainer");

					foreach (IProvider provider in ProviderManager.Providers)
					{
						ProviderInfo providerInfo = new ProviderInfo
						{
							Provider = provider
						};
						providersContainer.Add(providerInfo);
					}

					ListView rulesListView = rootElement.Q<ListView>("rulesList");
					ListView filtersListView = rootElement.Q<ListView>("filtersList");
					ListView actionsListView = rootElement.Q<ListView>("actionsList");

					//Filters
					{
						ToolbarMenu addFilterMenu = rootElement.Q<VisualElement>("filtersContainer").Q<ToolbarMenu>("addMenu");
						foreach (Type type in Filters.List)
						{
							addFilterMenu.menu.AppendAction(type.Name, x =>
							{
								if (settings.FindProperty("m_rules").arraySize < 1 || rulesListView.selectedIndex < 0)
								{
									return;
								}

								object instance = Activator.CreateInstance(type);
								if (instance != null)
								{
									SerializedProperty filters = settings.FindProperty("m_rules").GetArrayElementAtIndex(rulesListView.selectedIndex).FindPropertyRelative("m_filters");
									filters.InsertArrayElementAtIndex(filters.arraySize);

									SerializedProperty filter = filters.GetArrayElementAtIndex(filters.arraySize - 1);
									filter.managedReferenceValue = (IFilter)instance;

									settings.ApplyModifiedProperties();
								}
							});
						}

						ToolbarButton removeFilterBtn = rootElement.Q<VisualElement>("filtersContainer").Q<ToolbarButton>("removeBtn");

						removeFilterBtn.clicked += () =>
						{
							if (rulesListView.selectedIndex < 0 || filtersListView.selectedIndex < 0)
							{
								return;
							}
							settings.FindProperty("m_rules").GetArrayElementAtIndex(rulesListView.selectedIndex).FindPropertyRelative("m_filters").DeleteArrayElementAtIndex(filtersListView.selectedIndex);

							settings.ApplyModifiedProperties();
						};

						filtersListView.onSelectionChange += x =>
						{
							//Debug.Log("Change");
							settings.ApplyModifiedProperties();
							settings.Update();
							filtersListView.MarkDirtyRepaint();
						};

						filtersListView.RegisterCallback<DragExitedEvent>(x =>
						{
							filtersListView.MarkDirtyRepaint();
							settings.ApplyModifiedProperties();
						});

						filtersListView.makeItem = () =>
						{
							VisualElement container = new VisualElement();
							container.style.flexDirection = FlexDirection.Row;
							container.style.alignContent = Align.Center;

							PropertyField propertyField = new PropertyField();
							propertyField.style.alignSelf = Align.Center;
							propertyField.style.flexGrow = 1;

							container.Add(propertyField);

							return container;
						};

						filtersListView.bindItem = (e, i) =>
						{
							if (rulesListView.selectedIndex < 0 || rulesListView.selectedIndex >= settings.FindProperty("m_rules").arraySize)
							{
								return;
							}

							SerializedProperty filters = settings.FindProperty("m_rules").GetArrayElementAtIndex(rulesListView.selectedIndex).FindPropertyRelative("m_filters");

							PropertyField propertyField = e.Q<PropertyField>();

							if (i >= filters.arraySize)
							{
								propertyField.Unbind();
								return;
							}

							propertyField.BindProperty(filters.GetArrayElementAtIndex(i));
						};
					}

					//Actions
					{
						ToolbarMenu addActioMenu = rootElement.Q<VisualElement>("actionsContainer").Q<ToolbarMenu>("addMenu");
						foreach (var type in Actions.List)
						{
							addActioMenu.menu.AppendAction(type.Name, x =>
							{
								if (settings.FindProperty("m_rules").arraySize < 1 || rulesListView.selectedIndex < 0)
								{
									return;
								}

								object instance = Activator.CreateInstance(type);
								if (instance != null)
								{
									SerializedProperty actions = settings.FindProperty("m_rules").GetArrayElementAtIndex(rulesListView.selectedIndex).FindPropertyRelative("m_actions");
									actions.InsertArrayElementAtIndex(actions.arraySize);

									SerializedProperty action = actions.GetArrayElementAtIndex(actions.arraySize - 1);
									action.managedReferenceValue = (IAction)instance;

									settings.ApplyModifiedProperties();
								}
							});
						}

						ToolbarButton removeActionBtn = rootElement.Q<VisualElement>("actionsContainer").Q<ToolbarButton>("removeBtn");

						removeActionBtn.clicked += () =>
						{
							if (rulesListView.selectedIndex < 0 || actionsListView.selectedIndex < 0)
							{
								return;
							}
							settings.FindProperty("m_rules").GetArrayElementAtIndex(rulesListView.selectedIndex).FindPropertyRelative("m_actions").DeleteArrayElementAtIndex(actionsListView.selectedIndex);

							settings.ApplyModifiedProperties();
						};

						actionsListView.makeItem = () =>
						{
							VisualElement container = new VisualElement();
							container.style.flexDirection = FlexDirection.Row;
							container.style.alignContent = Align.Center;

							PropertyField propertyField = new PropertyField();
							propertyField.style.alignSelf = Align.Center;
							propertyField.style.flexGrow = 1;

							container.Add(propertyField);

							return container;
						};
						actionsListView.bindItem = (e, i) =>
						{

							if (rulesListView.selectedIndex < 0 || rulesListView.selectedIndex >= settings.FindProperty("m_rules").arraySize)
							{
								return;
							}

							SerializedProperty actions = settings.FindProperty("m_rules").GetArrayElementAtIndex(rulesListView.selectedIndex).FindPropertyRelative("m_actions");

							PropertyField propertyField = e.Q<PropertyField>();

							if (i >= actions.arraySize)
							{
								propertyField.Unbind();
								return;
							}

							propertyField.BindProperty(actions.GetArrayElementAtIndex(i));
						};
					}

					//Rules
					{
						rulesListView.makeItem = () =>
						{
							StyleSheet disablePropertyLabelStyle = Helpers.LoadDisablePropertyLabelStyle();

							VisualElement container = new VisualElement();
							container.style.flexDirection = FlexDirection.Row;
							container.style.alignContent = Align.Center;

							PropertyField isEnableToggle = new PropertyField();
							isEnableToggle.styleSheets.Add(disablePropertyLabelStyle);
							isEnableToggle.style.alignSelf = Align.Center;
							container.Add(isEnableToggle);

							Label label = new Label();
							label.style.alignSelf = Align.Center;
							container.Add(label);

							return container;
						};
						rulesListView.bindItem = (e, i) =>
						{
							if (i >= settings.FindProperty("m_rules").arraySize)
							{
								return;
							}
							e.Q<Label>().BindProperty(settings.FindProperty("m_rules").GetArrayElementAtIndex(i).FindPropertyRelative("m_name"));
							e.Q<PropertyField>().BindProperty(settings.FindProperty("m_rules").GetArrayElementAtIndex(i).FindPropertyRelative("m_enabled"));
						};

						ToolbarButton addRuleBtn = rootElement.Q<VisualElement>("rulesContainer").Q<ToolbarButton>("addBtn");

						SerializedProperty rulesProperty = settings.FindProperty("m_rules");

						addRuleBtn.clicked += () =>
						{
							rulesListView.Unbind();
							rulesProperty.InsertArrayElementAtIndex(rulesProperty.arraySize);
							Rule.Reset(rulesProperty.GetArrayElementAtIndex(rulesProperty.arraySize - 1));
							settings.ApplyModifiedProperties();
							rulesListView.BindProperty(settings.FindProperty("m_rules"));
						};

						ToolbarButton removeRuleBtn = rootElement.Q<VisualElement>("rulesContainer").Q<ToolbarButton>("removeBtn");
						removeRuleBtn.clicked += () =>
						{
							if (rulesListView.selectedIndex < 0)
							{
								return;
							}
							TextField ruleName = rootElement.Q<TextField>("ruleName");
							ruleName.Unbind();
							filtersListView.Unbind();
							actionsListView.Unbind();

							int indexToDelete = rulesListView.selectedIndex;

							rulesListView.Unbind();
							rulesProperty.DeleteArrayElementAtIndex(indexToDelete);
							settings.ApplyModifiedProperties();
							rulesListView.BindProperty(settings.FindProperty("m_rules"));
						};

						rulesListView.onSelectionChange += x =>
						{
							TextField ruleName = rootElement.Q<TextField>("ruleName");
							SerializedProperty rulesProperty = settings.FindProperty("m_rules");
							SerializedProperty selectedRuleProperty = rulesProperty.GetArrayElementAtIndex(rulesListView.selectedIndex);

							ruleName.Unbind();
							ruleName.BindProperty(selectedRuleProperty.FindPropertyRelative("m_name"));

							filtersListView.selectedIndex = -1; // Force deselect item to avoid null ref exception �\_(�-�)_/�
							filtersListView.Unbind();
							filtersListView.BindProperty(selectedRuleProperty.FindPropertyRelative("m_filters"));

							actionsListView.selectedIndex = -1; // Force deselect item to avoid null ref exception �\_(�-�)_/�
							actionsListView.Unbind();
							actionsListView.BindProperty(selectedRuleProperty.FindPropertyRelative("m_actions"));
						};

						rulesListView.BindProperty(settings.FindProperty("m_rules"));
					}

					//Advanced
					UUI.Foldout advancedContainer = rootElement.Q<UUI.Foldout>("advancedFoldout");

					s_createStandaloneServerBtn = advancedContainer.Q<Button>("createStandaloneServerBtn");
					s_createStandaloneServerBtn.clicked += OnCreateStandaloneServerBtnClicked;

					s_cleanStandaloneServerBtn = advancedContainer.Q<Button>("cleanStandaloneServerBtn");
					s_cleanStandaloneServerBtn.clicked += OnCleanStandaloneServerBtnClicked;

					s_deleteStandaloneServerBtn = advancedContainer.Q<Button>("deleteStandaloneServerBtn");
					s_deleteStandaloneServerBtn.clicked += OnDeleteStandaloneServerBtnClicked;

					s_clearUnityCacheBtn = advancedContainer.Q<Button>("clearUnityCacheBtn");
					s_clearUnityCacheBtn.SetEnabled(CacheManager.ClearIsRunning == false);
					s_clearUnityCacheBtn.clicked += OnClearUnityCacheBtnClicked;

					s_refreshServerCacheBtn = advancedContainer.Q<Button>("clearLocalServerCacheBtn");
					s_refreshServerCacheBtn.SetEnabled(CacheManager.ClearIsRunning == false);
					s_refreshServerCacheBtn.clicked += OnRefreshServerCacheBtnClicked;

					s_refreshBothCacheBtn = advancedContainer.Q<Button>("clearBothCacheBtn");
					s_refreshBothCacheBtn.SetEnabled(CacheManager.ClearIsRunning == false);
					s_refreshBothCacheBtn.clicked += OnRefreshBothCacheBtnClicked;

					s_saveForceSyncToggle = advancedContainer.Q<Toggle>("saveForceSyncToggle");

					rootElement.Bind(settings);
				},

				// Populate the search keywords to enable smart search filtering and label highlighting:
				keywords = sk_keywords
			};

			return provider;
		}

		private static void OnDisembeddingRequested()
		{
			EmbeddingManager.Disembed();
		}

		private static void OnEmbeddingRequested()
		{
			EmbeddingManager.Embed();
		}

		private static void OnDeleteStandaloneServerBtnClicked()
		{
			Core.Helpers.DeleteStandaloneServerProject();
		}

		private static void OnCleanStandaloneServerBtnClicked()
		{
			Core.Helpers.DeleteStandaloneServerProject();
			Core.Helpers.CreateStandaloneServerProject();
		}

		private static void OnCreateStandaloneServerBtnClicked()
		{
			Core.Helpers.CreateStandaloneServerProject();
		}

		private static async void OnRefreshBothCacheBtnClicked()
		{
			await CacheManager.ClearAsync(PackageCacheType.Both, s_saveForceSyncToggle.value);

			s_saveForceSyncToggle.value = false;
		}

		private static async void OnRefreshServerCacheBtnClicked()
		{
			await CacheManager.ClearAsync(PackageCacheType.LocalServer, s_saveForceSyncToggle.value);

			s_saveForceSyncToggle.value = false;
		}

		private static async void OnClearUnityCacheBtnClicked()
		{
			await CacheManager.ClearAsync(PackageCacheType.Upm, s_saveForceSyncToggle.value);

			s_saveForceSyncToggle.value = false;
		}

		private static async void OnUpdateRegistryBtnClicked()
		{
			await Registry.UpdateScopedRegistryAsync();
		}

		private static async void OnApplyBtnClicked()
		{
			s_applyBtn.SetEnabled(false);
			Server.Restart();
			await Registry.UpdateScopedRegistryAsync();
		}

		static LFusionLocalProviderSettingsUIElementsRegister()
		{
			Registry.UpdateScopedRegistryStarted += OnUpdateRegistryStarted;
			Registry.UpdateScopedRegistryEnded += Registry_UpdateRegistryEnded;

			CacheManager.ClearStarted += OnClearStarted;
			CacheManager.ClearEnded += OnClearEnded;
		}

		private static void OnClearEnded()
		{
			s_refreshBothCacheBtn?.SetEnabled(true);
			s_clearUnityCacheBtn?.SetEnabled(true);
			s_refreshServerCacheBtn?.SetEnabled(true);
		}

		private static void OnClearStarted()
		{
			s_refreshBothCacheBtn?.SetEnabled(false);
			s_clearUnityCacheBtn?.SetEnabled(false);
			s_refreshServerCacheBtn?.SetEnabled(false);
		}

		private static void Registry_UpdateRegistryEnded()
		{
			s_applyBtn?.SetEnabled(true);
			s_updateRegistryBtn?.SetEnabled(true);
		}

		private static void OnUpdateRegistryStarted()
		{
			s_applyBtn?.SetEnabled(false);
			s_updateRegistryBtn?.SetEnabled(false);
		}


	}
}