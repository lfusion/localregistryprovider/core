/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using LFusion.LocalRegistryProvider;
	using UnityEditor;
	using UnityEngine;
	using UnityEngine.UIElements;

	public static class Helpers
	{
		#region Contants
		private const string TITLE_ID = "titleLabel";
		private const string VERSION_ID = "versionLabel";
		private const string SETTINGS_CONTAINER_ID = "settingsContainer";
		
		private const string SETTINGS_UI_GUID = "07415e92f1e14d74e8d9685695507e8e";

		private const string SETTINGS_CONTAINER_FILENAME = "LFusionSettingsContainer.uxml";
		private const string DISABLE_PROPERTY_LABEL_STYLE_FILENAME = "DisablePropertyLabel.uss";

		private static readonly string k_visualTreesPath = AssetDatabase.GUIDToAssetPath(SETTINGS_UI_GUID) + "/VisualTrees/";
		private static readonly string k_stylesPath = AssetDatabase.GUIDToAssetPath(SETTINGS_UI_GUID) + "/Styles/";
		private static readonly string k_imagesPath = AssetDatabase.GUIDToAssetPath(SETTINGS_UI_GUID) + "/Images/";
		#endregion Constants

		#region Fields
		private static StyleSheet s_disablePropertyLabelStyle = null;
		#endregion Fields

		#region Methods
		#region Internal
		internal static Texture2D LoadUiImage(string path)
		{
			return AssetsLoader.LoadTexture2DAsset(k_imagesPath + path);
		}

		internal static StyleSheet LoadUiStyleSheet(string path)
		{
			return AssetDatabase.LoadAssetAtPath<StyleSheet>(k_stylesPath + path);
		}

		internal static VisualTreeAsset LoadUiVisualTree(string path)
		{
			return AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(k_visualTreesPath + path);
		}
		#endregion Internal
		#region Public
		public static VisualElement LoadSettingsContainer(VisualElement rootElement)
		{
			VisualTreeAsset main = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(k_visualTreesPath + SETTINGS_CONTAINER_FILENAME);

			main.CloneTree(rootElement);

			return rootElement.Q<VisualElement>(SETTINGS_CONTAINER_ID);
		}

		public static VisualElement LoadSettingsContainer(VisualElement rootElement, string title, string version = null)
		{
			VisualElement settingsContainer = LoadSettingsContainer(rootElement);

			SettingsContainerSetTitle(rootElement, title);
			SettingsContainerSetVersion(rootElement, version);

			return settingsContainer;
		}

		public static void SettingsContainerSetTitle(VisualElement rootElement, string title)
		{
			rootElement.Q<Label>(TITLE_ID).text = title;
		}

		public static void SettingsContainerSetVersion(VisualElement rootElement, string version)
		{
			Label versionLabel = rootElement.Q<Label>(VERSION_ID);
			if (version != null)
			{
				versionLabel.text = version;
			}
			else
			{
				versionLabel.text = "Dev";
			}
		}

		public static string SettingsPath(params string[] paths)
		{
			string path = "Project/" + Constants.PackageManagerSettingsName + "/" + Constants.PackageShortDisplayedName;

			for (int i = 0; i < paths.Length; i++)
			{
				path += "/" + paths[i];
			}

			return path;
		}

		public static void LoadSettingsTemplate(VisualElement rootElement, VisualTreeAsset visualTreeAsset)
		{
			VisualElement settingsContainer = rootElement.Q<VisualElement>(SETTINGS_CONTAINER_ID);

			visualTreeAsset.CloneTree(settingsContainer);
		}

		public static void LoadSettingsTemplate(VisualElement rootElement, string resourcePath)
		{
			VisualTreeAsset visualTreeAsset = Resources.Load<VisualTreeAsset>(resourcePath);

			LoadSettingsTemplate(rootElement, visualTreeAsset);
		}

		public static StyleSheet LoadDisablePropertyLabelStyle()
		{
			if (s_disablePropertyLabelStyle != null)
			{
				return s_disablePropertyLabelStyle;
			}

			s_disablePropertyLabelStyle = LoadUiStyleSheet(DISABLE_PROPERTY_LABEL_STYLE_FILENAME);

			return s_disablePropertyLabelStyle;
		}
		#endregion Public
		#endregion Methods
	}
}