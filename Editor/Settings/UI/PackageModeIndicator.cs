/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider.UI
{
	using UnityEngine;
	using UnityEngine.UIElements;

	public class PackageModeIndicator : BindableElement, INotifyValueChanged<int>
	{
		#region UIElement
		public new class UxmlFactory : UxmlFactory<PackageModeToggle, UxmlTraits> { }

		public new class UxmlTraits : BindableElement.UxmlTraits
		{
			readonly UxmlEnumAttributeDescription<EmbedState> k_value = new UxmlEnumAttributeDescription<EmbedState> { name = "value", defaultValue = EmbedState.Disembedded };

			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);

				if (ve is PackageModeToggle f)
				{
					//f.SetValueWithoutNotify(k_value.GetValueFromBag(bag, cc));
					f.value = (int)k_value.GetValueFromBag(bag, cc);
				}
			}
		}
		#endregion UIElement

		#region Constantes
		private const string TOOLTIP_DISEMBEDDED = "Local Registry Provider server running";
		private const string TOOLTIP_EMBEDDING = "Embedding packages installed from Local Registry Provider";
		private const string TOOLTIP_EMBEDDED = "All packages installed from Local Registry Provider are embedded";
		private const string TOOLTIP_DISEMBEDDING = "Disembedding packages installed from Local Registry Provider";

		private readonly Color COLOR_DISEMBEDDED = new Color(0, 0.51f, 0.706f, 1);
		private readonly Color COLOR_BUSY = new Color(1, 0.5f, 0, 1);
		private readonly Color COLOR_EMBEDDED = new Color(0, 1.0f, 0, 1);
		#endregion Constantes

		#region Fields
		private EmbedState m_embedState = EmbedState.Disembedded;

		private readonly Texture2D k_embeddingImage = null;
		private readonly Texture2D k_embeddedImage = null;
		private readonly Texture2D k_disembeddingImage = null;
		private readonly Texture2D k_disembeddedImage = null;

		private readonly Image k_stateImage = new Image() { name = "image-state", scaleMode = ScaleMode.ScaleToFit };

		private bool m_useColor = false;
		#endregion Fields

		#region Properties
		public int value { get => (int)m_embedState; set => ChangeEmbedStateValue((EmbedState)value); }
		public bool UseColor
		{
			get => m_useColor;
			set 
			{
				m_useColor = value;
				UpdateUI(); 
			}
		}
		#endregion Properties

		#region Methods
		#region Public
		public PackageModeIndicator()
		{
			k_embeddingImage = Helpers.LoadUiImage("embedding_32x32.png");
			k_embeddedImage = Helpers.LoadUiImage("embedded_32x32.png");
			k_disembeddingImage = Helpers.LoadUiImage("disembedding_32x32.png");
			k_disembeddedImage = Helpers.LoadUiImage("disembedded_32x32.png");

			int margin = 2;

			k_stateImage.style.marginTop = margin;
			k_stateImage.style.marginBottom = margin;
			k_stateImage.style.marginLeft = margin;
			k_stateImage.style.marginRight = margin;

			hierarchy.Add(k_stateImage);

			UpdateUI();
		}

		public void SetValueWithoutNotify(int newValue)
		{
			ChangeEmbedStateValue((EmbedState)newValue);
		}
		#endregion Public

		#region Private
		private void ChangeEmbedStateValue(EmbedState value)
		{
			if (m_embedState == value)
			{
				return;
			}
			m_embedState = value;

			UpdateUI();
		}

		private void UpdateUI()
		{
			Color tintColor;
			switch (m_embedState)
			{
				case EmbedState.Disembedded:
					k_stateImage.image = k_disembeddedImage;
					this.tooltip = TOOLTIP_DISEMBEDDED;
					tintColor = COLOR_DISEMBEDDED;
					break;
				case EmbedState.Embedding:
					k_stateImage.image = k_embeddingImage;
					this.tooltip = TOOLTIP_EMBEDDING;
					tintColor = COLOR_BUSY;
					break;
				case EmbedState.Embedded:
					k_stateImage.image = k_embeddedImage;
					this.tooltip = TOOLTIP_EMBEDDED;
					tintColor = COLOR_EMBEDDED;
					break;
				case EmbedState.Disembedding:
					k_stateImage.image = k_disembeddingImage;
					this.tooltip = TOOLTIP_DISEMBEDDING;
					tintColor = COLOR_BUSY;
					break;
				default:
					tintColor = Color.white;
					break;
			}

			if (UseColor)
			{
				k_stateImage.tintColor = tintColor;
			}
			else
			{
				k_stateImage.tintColor = Color.white;
			}
		}
		#endregion Private
		#endregion Methods
	}
}