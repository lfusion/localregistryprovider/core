/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Globalization;
	using UnityEngine;

	[Serializable]
	public struct ClearCacheRef : IToJson, IParseJson
	{
		[SerializeField] int _id;
		[SerializeField] string _date;

		public int ID { get => _id; }
		public DateTime GetDateTime()
		{
			if (DateTime.TryParse(_date, CultureInfo.GetCultureInfo("fr-FR"), DateTimeStyles.AssumeUniversal, out DateTime outDate))
			{
				return outDate;
			}

			return new DateTime(0);
		}

		public ClearCacheRef(int id, DateTime date)
		{
			_id = id;
			_date = date.ToString(CultureInfo.GetCultureInfo("fr-FR"));
		}

		public ClearCacheRef(int id) : this(id, DateTime.UtcNow)
		{

		}

		public string ToJson(bool pretty = false)
		{
			return JsonUtility.ToJson(this, pretty);
		}

		public void ParseJson(string json)
		{
			int idIndex = json.IndexOf("\"_id\"");

			if (idIndex > -1)
			{
				while (json[idIndex] != ':')
				{
					idIndex++;
				}

				int idEndValueIndex = idIndex;

				while (json[idEndValueIndex] != ',')
				{
					idEndValueIndex++;
				}

				string idValue = json.Substring(idIndex + 1, idEndValueIndex - idEndValueIndex + 1).Trim();

				int.TryParse(idValue, out _id);
			}

			int dateIndex = json.IndexOf("\"_date\"");

			if (dateIndex > -1)
			{

				while (json[dateIndex] != ':')
				{
					dateIndex++;
				}

				while (json[dateIndex] != '\"')
				{
					dateIndex++;
				}

				int dateEndIndex = dateIndex + 1;

				while (json[dateEndIndex] != '\"')
				{
					dateEndIndex++;
				}

				_date = json.Substring(dateIndex + 1, dateEndIndex - dateIndex - 1);
			}
		}

		public void Increment()
		{
			_id++;
			_date = DateTime.UtcNow.ToString(CultureInfo.GetCultureInfo("fr-FR"));
		}
	}
}