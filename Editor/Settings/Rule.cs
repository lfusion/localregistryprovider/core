/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEngine;

	[Serializable]
	public class Rule
	{
		#region Fields
		[SerializeField] private string m_name = "Rule";
		[SerializeField] private bool m_enabled = true;

		[SerializeReference] private List<IFilter> m_filters;
		[SerializeReference] private List<IAction> m_actions;
		#endregion Fields

		#region Propertiers
		public string Name { get => m_name; }
		public bool Enabled { get => m_enabled; }
		#endregion Propertiers

		#region Methods
		public void Execute(PackageVersionInfo packageVersionInfo)
		{
			if (m_enabled && Filters.Validate(packageVersionInfo, m_filters))
			{
				Actions.Execute(packageVersionInfo, m_actions);
			}
		}

		public static void Reset(SerializedProperty property)
		{
			if (property != null && property.type == typeof(Rule).Name)
			{
				property.FindPropertyRelative("m_name").stringValue = "Rule";
				property.FindPropertyRelative("m_filters").ClearArray();
				property.FindPropertyRelative("m_actions").ClearArray();
			}
		}
		#endregion Methods
	}

	public static class RulesExtention
	{
		public static void Execute(this IList<Rule> rules, PackageVersionInfo packageVersionInfo)
		{
			for (int i = 0; i < rules.Count; i++)
			{
				rules[i].Execute(packageVersionInfo);
			}
		}
	}
}