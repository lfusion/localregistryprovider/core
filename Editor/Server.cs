/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System.Collections.Generic;
	using System.Net;
	using System.Threading.Tasks;
	using System.Text;
	using UnityEditor;
	using System.Threading;
	using System.IO;
	using System;
	using Microsoft.Win32;

	[InitializeOnLoad]
	public static class Server
	{
		#region Constantes
		private const string LOG_PREFIX = "<color=orange>[Core-Server]</color>";
		#endregion Constantes

		#region Fields
		private static HttpListener s_httpListener;
		private static Thread s_serverThread = new Thread(new ThreadStart(HandleIncomingConnections));
		private static int s_currentPort = -1;
		private static readonly SemaphoreSlim s_stopSignal = new SemaphoreSlim(0, 1);
		#endregion Fields

		#region Properties
		public static string Uri { get => "http://localhost:" + CurrentPort + "/"; }
		private static string UriPrefix { get => "http://*:" + CurrentPort + "/"; }

		private static int CurrentPort { get => s_currentPort > 0 ? s_currentPort : Settings.instance.ServerPort; }
		#endregion Properties

		#region Methods

		#region Public
		/// <summary>
		/// Start registry Server
		/// </summary>
		/// <returns>Success</returns>
		public static bool Start()
		{
			if (s_serverThread.IsAlive)
			{
				Log("Server is already started");
				return false;
			}

			Log("Starting Registry Provider server");

			s_currentPort = Settings.instance.ServerPort;

			CreateAndInitServerThread();
			InitHttpListener();

			try
			{
				s_httpListener.Start();
			}
			catch (Exception e)
			{
				LogError(e.Message);
				return false;
			}

			Log("Registry Provider server is started");

			s_serverThread.Start();

			return true;
		}

		/// <summary>
		/// Stop registry server if running
		/// </summary>
		public static void Stop()
		{
			if (s_httpListener != null && s_httpListener.IsListening)
			{
				Log("Stoping Registry Provider");
				s_stopSignal.Release();
				s_serverThread.Join();
				s_httpListener.Abort();
				s_httpListener.Prefixes.Clear();
				s_httpListener.Close();
				s_httpListener = null;
				s_currentPort = -1;
			}
		}

		/// <summary>
		/// Restart server if current state allow it
		/// </summary>
		public static void Restart()
		{
			Stop();
			if (EmbedStateRequireServer(Settings.instance.EmbedState))
			{
				Start();
			}
		}
		#endregion Public

		#region Private
		static Server()
		{
			AssemblyReloadEvents.beforeAssemblyReload -= OnBeforeAssemblyReloaded;
			AssemblyReloadEvents.beforeAssemblyReload += OnBeforeAssemblyReloaded;

			AssemblyReloadEvents.afterAssemblyReload -= OnAfterAssemblyReloaded;
			AssemblyReloadEvents.afterAssemblyReload += OnAfterAssemblyReloaded;

			Settings.instance.EmbedStateChanged += OnEmbedStateChanged;
			SystemEvents.PowerModeChanged += OnPowerChange;
		}

		private static void CreateAndInitServerThread()
		{
			if (!string.IsNullOrEmpty(s_serverThread.Name))
			{
				s_serverThread = new Thread(new ThreadStart(HandleIncomingConnections));
			}

			s_serverThread.Name = "LFusion Local Provider";
		}

		private static void InitHttpListener()
		{
			if (s_httpListener == null)
			{
				s_httpListener = new HttpListener();
			}
			else if (s_httpListener.IsListening)
			{
				s_httpListener.Abort();
				s_httpListener.Prefixes.Clear();
			}

			s_httpListener.Prefixes.Add(UriPrefix);
		}

		private static void OnAfterAssemblyReloaded()
		{
			if (EmbedStateRequireServer(Settings.instance.EmbedState))
			{
				Start();
			}
		}

		private static void OnBeforeAssemblyReloaded()
		{
			Stop();
		}

		private static bool EmbedStateRequireServer(EmbedState embedState)
		{
			switch (embedState)
			{
				case EmbedState.Disembedded:
				case EmbedState.Embedding:
				case EmbedState.Disembedding:
				default:
					return true;
				case EmbedState.Embedded:
					return false;
			}
		}

		private static async void HandleIncomingConnections()
		{
			Task stopEventWaiter = s_stopSignal.WaitAsync();

			while (s_httpListener.IsListening)
			{
				Task<HttpListenerContext> taskContext = s_httpListener.GetContextAsync();

				// Will wait here until we hear from a connection or stop event triggered
				await Task.WhenAny(taskContext, stopEventWaiter);

				if (stopEventWaiter.IsCompleted)
				{
					break;
				}

				HttpListenerContext ctx = taskContext.Result;

				// Peel out the requests and response objects
				HttpListenerRequest req = ctx.Request;
				HttpListenerResponse resp = ctx.Response;

				//Print out some info about the request
				//Log(string.Format("Url: {0}\nHttpMethod: {1}\nUserHostName: {2}\nUserAgen: {3}", req.Url.ToString(), req.HttpMethod, req.UserHostName, req.UserAgent));
				Log(string.Format("Request on: {0}", req.Url.PathAndQuery.ToString()));

				bool success = await ProcessRequest(ctx);

				byte[] data;

				// Write the response info
				if (success == false)
				{
					data = Encoding.UTF8.GetBytes("{\"error\":404}");
					resp.StatusCode = (int)HttpStatusCode.NotFound;
					resp.ContentType = "application/json";
					resp.ContentEncoding = Encoding.UTF8;
					resp.ContentLength64 = data.LongLength;

					await resp.OutputStream.WriteAsync(data, 0, data.Length);
				}

				// Write out to the response stream (asynchronously), then close it
				resp.Close();
			}
		}

		private static async Task<bool> ProcessRequest(HttpListenerContext context)
		{
			string[] path = context.Request.Url.Segments;

			for (int i = 0; i < path.Length; i++)
			{
				path[i] = path[i].Replace("/", "");
			}

			if (path.Length == 1)
			{
				await JsonResponse(context.Response, "{\"registry\":\"LFusion Local Registry Provider\"}");
				return true;
			}
			else if (path.Length == 2)
			{
				var packages = ProviderManager.GetPackages();

				if (packages.ContainsKey(path[1]))
				{
					await JsonResponse(context.Response, packages[path[1]].ToJson());
					return true;
				}
			}
			else if (path.Length == 3)
			{
				// host/-/all
				if (path[0] == "" &&
					path[1] == "-" &&
					path[2].ToLower() == "all")
				{
					await JsonResponse(context.Response, ProviderManager.GetPackages().ToJson().Insert(1, "\"_updated\":99999,"));
					return true;
				}

				var packages = ProviderManager.GetPackages();

				// host/[Package]/[Version]
				if (packages.ContainsKey(path[1]))
				{
					if (packages[path[1]].Versions.ContainsKey(path[2]))
					{
						await JsonResponse(context.Response, packages[path[1]].Versions[path[2]].ToJson());
						return true;
					}
				}
			}
			else if (path.Length == 4)
			{
				var packages = ProviderManager.GetPackages();

				// host/-/v1/search
				if (path[0] == "" &&
					path[1] == "-" &&
					path[2] == "v1" &&
					path[3] == "search")
				{
					string search = context.Request.QueryString.Get("text");
					int.TryParse(context.Request.QueryString.Get("from"), out int from);
					int.TryParse(context.Request.QueryString.Get("size"), out int size);

					if (size == 0)
					{
						size = 250;
					}

					List<SearchResponseItem> searchResponseItems = new List<SearchResponseItem>();

					int index = 0;

					foreach (var package in packages)
					{
						if (package.Key.StartsWith(search))
						{
							if (index >= from && index < from + size)
							{
								var responseItem = new SearchResponseItem(new SearchResponsePackageInfo(package.Value));
								searchResponseItems.Add(responseItem);
							}

							index++;
						}
					}

					SearchResponse searchResponse = new SearchResponse(searchResponseItems);

					await JsonResponse(context.Response, searchResponse.ToJson());
					return true;
				}
				// host/[Package]/[Version]/[File]
				else if (packages.ContainsKey(path[1]))
				{
					if (packages[path[1]].Versions.ContainsKey(path[2]))
					{
						string localPath = ProviderManager.GetPackageLocation(packages[path[1]].Versions[path[2]]);

						if (localPath != null)
						{
							await TgzResponse(
								context.Response,
								localPath
								);
							return true;
						}
					}
				}

			}

			return false;
		}

		private static async Task JsonResponse(HttpListenerResponse response, string json)
		{
			byte[] data;

			// Write the response info
			if (string.IsNullOrEmpty(json) == false)
			{
				data = Encoding.UTF8.GetBytes(json);
			}
			else
			{
				data = Encoding.UTF8.GetBytes("{\"error\":404}");
				response.StatusCode = (int)HttpStatusCode.NotFound;
			}
			response.ContentType = "application/json";
			response.ContentEncoding = Encoding.UTF8;
			response.ContentLength64 = data.LongLength;

			// Write out to the response stream (asynchronously), then close it
			await response.OutputStream.WriteAsync(data, 0, data.Length);
		}

		private static async Task TgzResponse(HttpListenerResponse response, string file)
		{
			if (File.Exists(file))
			{
				try
				{
					using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
					{
						// get mime type
						response.ContentType = "application/gzip";
						response.ContentLength64 = stream.Length;

						// copy file stream to response
						await stream.CopyToAsync(response.OutputStream);
						await stream.FlushAsync();
					}

					await response.OutputStream.FlushAsync();
				}
				catch (IOException e)
				{
					if (e.InnerException is System.Net.Sockets.SocketException)
					{
						LogWarning(string.Format("Sending {0} failed: {1}/n{2}", file, e.Message, e.StackTrace));
					}
					else
					{
						LogError(string.Format("Sending {0} failed: {1}/n{2}", file, e.Message, e.StackTrace));
					}
				}
				catch (Exception e)
				{
					LogError(string.Format("Sending {0} failed: {1}/n{2}", file, e.Message, e.StackTrace));
					try
					{
						response.StatusCode = (int)HttpStatusCode.InternalServerError;
					}
					catch (Exception)
					{
						//Just ignore
					}
				}
			}
			else
			{
				LogError(string.Format("File not found: {0}", file));
				response.StatusCode = (int)HttpStatusCode.NotFound;
			}
		}

		#region Callbacks
		private static void OnEmbedStateChanged(EmbedState embedMode)
		{
			switch (embedMode)
			{
				case EmbedState.Disembedded:
				case EmbedState.Embedding:
				case EmbedState.Disembedding:
				default:
					if (s_httpListener == null || s_httpListener.IsListening == false)
					{
						Start();
					}
					break;
				case EmbedState.Embedded:
					Stop();
					break;
			}
		}

		private static void OnPowerChange(object s, PowerModeChangedEventArgs e)
		{
			switch (e.Mode)
			{
				case PowerModes.Resume:
					if (EmbedStateRequireServer(Settings.instance.EmbedState))
					{
						Start();
					}
					break;
				case PowerModes.Suspend:
					Stop();
					break;
			}
		}
		#endregion Callbacks

		#region Debugs
#pragma warning disable IDE0051 // Supprimer les membres priv�s non utilis�s
		private static void Log(string log)
		{
			Debug.Log(log, LOG_PREFIX);
		}

		private static void LogWarning(string log)
		{
			Debug.LogWarning(log, LOG_PREFIX);
		}

		private static void LogError(string log)
		{
			Debug.LogError(log, LOG_PREFIX);
		}
#pragma warning restore IDE0051 // Supprimer les membres priv�s non utilis�s

		#endregion Debugs

		#endregion Private

		#endregion Methods
	}
}