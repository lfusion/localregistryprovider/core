/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barb�
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System.Threading.Tasks;

	/// <summary>
	/// Provider Interface.
	/// Implement it to add a provider.
	/// </summary>
	public interface IProvider
	{
		/// <summary>
		/// Name of the provider.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Description of the provider.
		/// </summary>
		public string Description { get; }

		/// <summary>
		/// PackageInfo array of packages provided.
		/// </summary>
		public PackageInfo[] Packages { get; }

		/// <summary>
		/// Must be set to <c>true</c> if <see cref="Packages"/> was changed by <see cref="Refresh(bool)"/> or <see cref="RefreshAsync(bool)"/>.
		/// </summary>
		public bool PackagesHasChanged { get; set; }

		/// <summary>
		/// Update <see cref="Packages"/> array.
		/// </summary>
		/// <param name="force">request to ignore cache if used</param>
		public void Refresh(bool force = false);

		/// <summary>
		/// Update <see cref="Packages"/> array asynchronously.
		/// </summary>
		/// <param name="force">request to ignore cache if used</param>
		/// <returns>asynchronous task</returns>
		public Task RefreshAsync(bool force = false);

		/// <summary>
		/// Opens provider settings.
		/// </summary>
		public void OpenSettings();

		/// <summary>
		/// Returns the path of the tgz file corresponding to the requested package and version.
		/// </summary>
		/// <param name="packageVersionInfo">Package information</param>
		/// <returns>The path of the Tgz file</returns>
		public string GetLocalPath(PackageVersionInfo packageVersionInfo);
	}
}
