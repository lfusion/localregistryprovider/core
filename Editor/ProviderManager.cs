﻿/*
*	LFusion Local Registry Provider is used to create a registry provider linked to the project.
*	Packages can be added by extensions implementing a provider.
*	Copyright (C) 2024  Lucas Barbé
*	
*	This program is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*	
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License
*	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace LFusion.LocalRegistryProvider
{
	using System;
	using System.Collections.Concurrent;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;
	using UnityEditor;

	[InitializeOnLoad]
	public static class ProviderManager
	{
		#region Class
		private class RefreshProviderOperation
		{
			private Queue<TaskCompletionSource<object>> m_taskCompletionSources = new Queue<TaskCompletionSource<object>>();
			private bool m_force = false;
			private DateTime m_execTime = DateTime.UtcNow.AddMilliseconds(REFRESH_BATCHING_DELAY);

			public readonly IProvider Provider;
			public bool Force { get => m_force; }
			public DateTime ExecTime { get => m_execTime; }
			public Queue<TaskCompletionSource<object>> TaskCompletionSources { get => m_taskCompletionSources; }

			public RefreshProviderOperation(IProvider provider)
			{
				Provider = provider;
			}

			public Task Wait(bool force)
			{
				m_force |= force;
				m_execTime = DateTime.UtcNow.AddMilliseconds(REFRESH_BATCHING_DELAY);
				TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
				m_taskCompletionSources.Enqueue(tcs);

				return tcs.Task;
			}

			public void End()
			{
				while (m_taskCompletionSources.Count > 0)
				{
					m_taskCompletionSources.Dequeue().SetResult(null);
				}
			}
		}
		#endregion Class

		#region Constants

		const string LOG_PREFIX = "<color=orange>[Core-ProviderManager]</color>";
		private const uint REFRESH_BATCHING_DELAY = 500;

		#endregion Constants

		#region Fields
		private static readonly List<IProvider> sk_providers = new List<IProvider>();
		private static readonly ConcurrentDictionary<PackageVersionInfo, IProvider> sk_packagesSource = new ConcurrentDictionary<PackageVersionInfo, IProvider>();
		private static readonly ConcurrentDictionary<string, PackageInfo> sk_fetchedPackages = new ConcurrentDictionary<string, PackageInfo>();
		private static readonly ConcurrentDictionary<IProvider, RefreshProviderOperation> sk_pendingOperations = new ConcurrentDictionary<IProvider, RefreshProviderOperation>();
		private static RefreshProviderOperation s_currentOperation = null;

		private static Task s_refreshProviderTask = null;
		#endregion Fields

		#region Properties
		public static IReadOnlyList<IProvider> Providers { get => sk_providers; }
		public static bool IsRefreshingProvider { get => s_refreshProviderTask != null; }
		#endregion Properties

		#region Events
		private static Action<bool> a_isRefreshingProviderHasChanged;
		public static event Action<bool> IsRefreshingProviderHasChanged
		{
			add
			{
				a_isRefreshingProviderHasChanged -= value;
				a_isRefreshingProviderHasChanged += value;
			}
			remove
			{
				a_isRefreshingProviderHasChanged -= value;
			}
		}
		#endregion Events

		#region Methods
		static ProviderManager()
		{
			var iProviderType = typeof(IProvider);
			var types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => iProviderType.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);

			foreach (var type in types)
			{
				var instance = Activator.CreateInstance(type);
				if (instance != null)
				{
					IProvider provider = (IProvider)instance;
					sk_providers.Add(provider);
					provider.Refresh();
					sk_pendingOperations.TryAdd(provider, null);
				}
			}

			string pList = string.Format("{0} Providers found: ↵", sk_providers.Count);
			foreach (var item in sk_providers)
			{
				pList += "\n\t" + item.Name;
			}

			Debug.Log(pList, LOG_PREFIX);
		}

		private static async void InitAsync()
		{
			var iProviderType = typeof(IProvider);
			var types = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => iProviderType.IsAssignableFrom(p) && !p.IsInterface && !p.IsAbstract);

			foreach (var type in types)
			{
				var instance = Activator.CreateInstance(type);
				if (instance != null)
				{
					sk_providers.Add((IProvider)instance);
					await ((IProvider)instance).RefreshAsync();
				}
			}

			string pList = "Local Registry Providers:";
			foreach (var item in sk_providers)
			{
				pList += "\n\t" + item.Name;
			}

			Debug.Log(pList);
		}

		public static PackageVersionInfo GetPackageVersionInfo(string packageName, string version)
		{
			var packages = GetPackages();

			if (packages.ContainsKey(packageName))
			{
				if (packages[packageName].Versions.ContainsKey(version))
				{
					return packages[packageName].Versions[version];
				}
			}

			return null;
		}

		public static IDictionary<string, PackageInfo> GetPackages()
		{
			//RefreshAll();

			if (sk_providers.Any(x => x.PackagesHasChanged) == false)
			{
				return sk_fetchedPackages;
			}

			sk_fetchedPackages.Clear();
			sk_packagesSource.Clear();

			foreach (var provider in sk_providers)
			{
				var packFromProvider = provider.Packages;

				foreach (var package in packFromProvider)
				{
					if (sk_fetchedPackages.ContainsKey(package.Name) == false)
					{
						foreach (var version in package.Versions)
						{
							Settings.instance.Rules.Execute(version.Value);
							if (string.IsNullOrEmpty(version.Value.Dist.Tarball))
							{
								version.Value.GenerateDistTarballUrl(Server.Uri);
							}
							sk_packagesSource.TryAdd(version.Value, provider);
						}

						sk_fetchedPackages.TryAdd(package.Name, package);
					}
					else
					{
						Debug.LogWarning(string.Format("Package {0} is already listed {0} from {1} was skipped", package.Name, provider.Name), LOG_PREFIX);
					}
				}

				provider.PackagesHasChanged = false;
			}

			return sk_fetchedPackages;
		}

		public static string GetPackageLocation(PackageVersionInfo packageVersionInfo)
		{
			if (sk_packagesSource.TryGetValue(packageVersionInfo, out IProvider provider))
			{
				return provider.GetLocalPath(packageVersionInfo);
			}

			return null;
		}

		public static IProvider GetPackageProvider(PackageVersionInfo packageVersionInfo)
		{
			if (sk_packagesSource.TryGetValue(packageVersionInfo, out IProvider provider))
			{
				return provider;
			}

			return null;
		}

		public static ProviderType GetProvider<ProviderType>() where ProviderType : class
		{
			if (typeof(IProvider).IsAssignableFrom(typeof(ProviderType)))
			{
				for (int i = 0; i < sk_providers.Count; i++)
				{
					if (sk_providers[i] is ProviderType provider)
					{
						return provider;
					}
				}
			}

			return null;
		}

		public static async Task RefreshProviderAsync<ProviderType>(bool force = false) where ProviderType : class
		{
			if (!(GetProvider<ProviderType>() is IProvider provider))
			{
				return;
			}

			await QueueRefreshProviderAndWait(provider, force);
		}

		public static void RefreshAll(bool force = false)
		{
			for (int i = 0; i < sk_providers.Count; i++)
			{
				sk_providers[i].Refresh(force);
			}
		}

		public static async Task RefreshAllAsync(bool force = false)
		{
			Task[] tasks = new Task[sk_providers.Count];
			for (int i = 0; i < sk_providers.Count; i++)
			{
				tasks[i] = QueueRefreshProviderAndWait(sk_providers[i], force);
			}
			await Task.WhenAll(tasks);
		}

		#region RefreshProviderSystem
		private static async void LaunchRefreshOperationExecutor()
		{
			if (s_refreshProviderTask == null)
			{
				s_refreshProviderTask = RefreshProvidersExecutorAsync();
				TriggerIsRefreshingProviderHasChanged(IsRefreshingProvider);
				await s_refreshProviderTask;
				s_refreshProviderTask = null;
				TriggerIsRefreshingProviderHasChanged(IsRefreshingProvider);
			}
		}

		private static async Task RefreshProvidersExecutorAsync()
		{
			bool operationPending = true;
			while (operationPending)
			{
				operationPending = false;
				foreach (RefreshProviderOperation operation in sk_pendingOperations.Values)
				{
					if (operation != null)
					{
						operationPending = true;
						if (operation.ExecTime < DateTime.UtcNow)
						{
							s_currentOperation = operation;
							sk_pendingOperations[operation.Provider] = null;
							await s_currentOperation.Provider.RefreshAsync(s_currentOperation.Force);
							await Registry.UpdateScopedRegistryAsync();
							await Task.Delay(100);
							s_currentOperation.End();
							s_currentOperation = null;
							break;
						}
					}
				}
				await Task.Yield();
			}
		}

		private static Task QueueRefreshProviderAndWait(IProvider provider, bool force)
		{
			if (sk_pendingOperations[provider] == null)
			{
				sk_pendingOperations[provider] = new RefreshProviderOperation(provider);
			}

			LaunchRefreshOperationExecutor();
			return sk_pendingOperations[provider].Wait(force);
		}

		private static void TriggerIsRefreshingProviderHasChanged(bool value)
		{
			a_isRefreshingProviderHasChanged?.Invoke(value);
		}
		#endregion RefreshProviderSystem
		#endregion Methods
	}
}
